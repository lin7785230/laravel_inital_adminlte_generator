<?php

namespace App\DataTables;

use App\Models\User;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class UsersDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)->addColumn('check', '<input type="checkbox" name="selected_users[]" value="{{ $id }}">');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(User $model)
    {
        return $model->newQuery()->select('id', 'name', 'email', 'created_at', 'updated_at');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->addCheckbox()
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id',
            'name' => $this->defaultColumn('name', true, true),
            'email' => $this->defaultColumn('email', true),
            'created_at' => $this->defaultColumn('created_at'),
            'updated_at' => $this->defaultColumn('updated_at')
        ];
    }

    protected function defaultColumn($value, $searchable = false, $orderable = false)
    {
        $attributes = [
            'name'  => $value,
            'data'  => $value,
            'title' => $this->builder()->getQualifiedTitle($value),
            'searchable' => $searchable,
            'orderable' => $orderable,
        ];

        return new Column($attributes);
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Users_' . date('YmdHis');
    }
}
