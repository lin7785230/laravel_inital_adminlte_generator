<?php

namespace App\Observers;

use App\Models\SendingEmail;

// creating, created, updating, updated, saving,
// saved,  deleting, deleted, restoring, restored

class SendingEmailObserver
{
    public function creating(SendingEmail $sending_email)
    {
        //
    }

    public function updating(SendingEmail $sending_email)
    {
        //
    }
}