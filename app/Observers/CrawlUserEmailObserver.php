<?php

namespace App\Observers;

use App\Models\CrawlUserEmail;

// creating, created, updating, updated, saving,
// saved,  deleting, deleted, restoring, restored

class CrawlUserEmailObserver
{
    public function creating(CrawlUserEmail $crawl_user_email)
    {
        //
    }

    public function updating(CrawlUserEmail $crawl_user_email)
    {
        //
    }
}