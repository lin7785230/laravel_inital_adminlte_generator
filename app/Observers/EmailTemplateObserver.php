<?php

namespace App\Observers;

use App\Models\EmailTemplate;

// creating, created, updating, updated, saving,
// saved,  deleting, deleted, restoring, restored

class EmailTemplateObserver
{
    public function creating(EmailTemplate $email_template)
    {
        //
    }

    public function updating(EmailTemplate $email_template)
    {
        //
    }
}