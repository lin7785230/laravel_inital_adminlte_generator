<?php

namespace App\Observers;

use App\Models\SpiderAccount;

// creating, created, updating, updated, saving,
// saved,  deleting, deleted, restoring, restored

class SpiderAccountObserver
{
    public function creating(SpiderAccount $spider_account)
    {
        //
    }

    public function updating(SpiderAccount $spider_account)
    {
        //
    }
}