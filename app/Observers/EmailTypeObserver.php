<?php

namespace App\Observers;

use App\Models\EmailType;

// creating, created, updating, updated, saving,
// saved,  deleting, deleted, restoring, restored

class EmailTypeObserver
{
    public function creating(EmailType $email_type)
    {
        //
    }

    public function updating(EmailType $email_type)
    {
        //
    }
}