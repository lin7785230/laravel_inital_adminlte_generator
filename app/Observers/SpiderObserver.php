<?php

namespace App\Observers;

use App\Models\Spider;

// creating, created, updating, updated, saving,
// saved,  deleting, deleted, restoring, restored

class SpiderObserver
{
    public function creating(Spider $spider)
    {
        //
    }

    public function updating(Spider $spider)
    {
        //
    }
}