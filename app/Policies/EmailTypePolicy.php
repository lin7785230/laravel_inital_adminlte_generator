<?php

namespace App\Policies;

use App\Models\User;
use App\Models\EmailType;

class EmailTypePolicy extends Policy
{
    public function update(User $user, EmailType $email_type)
    {
        // return $email_type->user_id == $user->id;
        return true;
    }

    public function destroy(User $user, EmailType $email_type)
    {
        return true;
    }
}
