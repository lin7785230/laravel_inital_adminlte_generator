<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Spider;

class SpiderPolicy extends Policy
{
    public function update(User $user, Spider $spider)
    {
        // return $spider->user_id == $user->id;
        return true;
    }

    public function destroy(User $user, Spider $spider)
    {
        return true;
    }
}
