<?php

namespace App\Policies;

use App\Models\User;
use App\Models\EmailTemplate;

class EmailTemplatePolicy extends Policy
{
    public function update(User $user, EmailTemplate $email_template)
    {
        // return $email_template->user_id == $user->id;
        return true;
    }

    public function destroy(User $user, EmailTemplate $email_template)
    {
        return true;
    }
}
