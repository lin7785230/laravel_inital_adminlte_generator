<?php

namespace App\Policies;

use App\Models\User;
use App\Models\SpiderAccount;

class SpiderAccountPolicy extends Policy
{
    public function update(User $user, SpiderAccount $spider_account)
    {
        // return $spider_account->user_id == $user->id;
        return true;
    }

    public function destroy(User $user, SpiderAccount $spider_account)
    {
        return true;
    }
}
