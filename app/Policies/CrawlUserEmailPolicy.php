<?php

namespace App\Policies;

use App\Models\User;
use App\Models\CrawlUserEmail;

class CrawlUserEmailPolicy extends Policy
{
    public function update(User $user, CrawlUserEmail $crawl_user_email)
    {
        // return $crawl_user_email->user_id == $user->id;
        return true;
    }

    public function destroy(User $user, CrawlUserEmail $crawl_user_email)
    {
        return true;
    }
}
