<?php

namespace App\Policies;

use App\Models\User;
use App\Models\SendingEmail;

class SendingEmailPolicy extends Policy
{
    public function update(User $user, SendingEmail $sending_email)
    {
        // return $sending_email->user_id == $user->id;
        return true;
    }

    public function destroy(User $user, SendingEmail $sending_email)
    {
        return true;
    }
}
