<?php

namespace App\Console;

use App\Jobs\CrawlAmazonReviewerEmail;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //$schedule->job(new CrawlAmazonReviewerEmail())->everyFifteenMinutes()->withoutOverlapping()->runInBackground()->sendOutputTo('/tmp/output.txt');
        # $schedule->exec('cd /var/www/0-python/tutorial/;PATH=$PATH:/usr/local/bin;export PATH;scrapy crawl amazon_reviewer -o B01M14SU9A.json -a product_id=B01M14SU9A -a max_page_no=10')->everyMinute()->withoutOverlapping();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
