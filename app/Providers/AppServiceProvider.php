<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
	{
		\App\Models\User::observe(\App\Observers\UserObserver::class);
		\App\Models\SpiderAccount::observe(\App\Observers\SpiderAccountObserver::class);
		\App\Models\CrawlUserEmail::observe(\App\Observers\CrawlUserEmailObserver::class);
		\App\Models\Spider::observe(\App\Observers\SpiderObserver::class);
		\App\Models\Product::observe(\App\Observers\ProductObserver::class);
		\App\Models\Tag::observe(\App\Observers\TagObserver::class);
		\App\Models\EmailType::observe(\App\Observers\EmailTypeObserver::class);
		\App\Models\SendingEmail::observe(\App\Observers\SendingEmailObserver::class);
		\App\Models\EmailTemplate::observe(\App\Observers\EmailTemplateObserver::class);
		\App\Models\Member::observe(\App\Observers\MemberObserver::class);

        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() !== 'production') {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }

        if (app()->environment() == 'local' || app()->environment() == 'testing') {
            $this->app->register(\Summerblue\Generator\GeneratorsServiceProvider::class);
        }
    }
}
