<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
		 \App\Models\SpiderAccount::class => \App\Policies\SpiderAccountPolicy::class,
		 \App\Models\CrawlUserEmail::class => \App\Policies\CrawlUserEmailPolicy::class,
		 \App\Models\Spider::class => \App\Policies\SpiderPolicy::class,
		 \App\Models\Product::class => \App\Policies\ProductPolicy::class,
		 \App\Models\Tag::class => \App\Policies\TagPolicy::class,
		 \App\Models\EmailType::class => \App\Policies\EmailTypePolicy::class,
		 \App\Models\SendingEmail::class => \App\Policies\SendingEmailPolicy::class,
		 \App\Models\EmailTemplate::class => \App\Policies\EmailTemplatePolicy::class,
		 \App\Models\Member::class => \App\Policies\MemberPolicy::class,
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
