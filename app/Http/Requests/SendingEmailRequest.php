<?php

namespace App\Http\Requests;


class SendingEmailRequest extends Request
{
    public function rules()
    {
        switch ($this->method()) {
            // CREATE
            case 'POST': {
                return [
                    'member_ids' => 'required',
                    'product_ids' => 'required',
                    'template_id' => 'required',
                ];
            }
            // UPDATE
            case 'PUT':
            case 'PATCH': {
                return [

                ];
            }
            case 'GET':
            case 'DELETE':
            default: {
                return [];
            }
        }
    }

    public function messages()
    {
        return [
            // Validation messages
        ];
    }
}
