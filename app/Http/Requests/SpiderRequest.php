<?php

namespace App\Http\Requests;


class SpiderRequest extends Request
{
    public function rules()
    {
        switch($this->method()) {
            // CREATE
            case 'POST': {
                return [
                    'name' => 'required|max:255',
                    'sku' => 'required|max:255',
                    'sort_by' => 'required',
                    'filter_by_star' => 'required',
                    'plant' => 'date',
                ];
            }
            // UPDATE
            case 'PUT':
            case 'PATCH': {
                return [
                    'name' => 'required|max:255',
                    'content' => 'required|max:255',
                    'plant' => 'date',
                ];
            }
            case 'GET':
            case 'DELETE':
            default: {
                return [];
            }
        }
    }

    public function messages()
    {
        return [
            // Validation messages
        ];
    }
}
