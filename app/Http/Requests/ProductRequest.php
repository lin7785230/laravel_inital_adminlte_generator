<?php

namespace App\Http\Requests;


class ProductRequest extends Request
{
    public function rules()
    {
        switch($this->method()) {
            // CREATE
            case 'POST': {
                return [
                    'sku' => 'required|unique:products|max:255',
                    'name' => 'required|max:255',
                    'desc' => 'required|max:255',
                    'price' => 'required|numeric',
                    'promotion_price' => 'required|numeric',
                    'picture_url' => 'required|max:255',
                    'link_url' => 'required|max:255',
                    'notes' => 'required|max:255',
                ];
            }
            // UPDATE
            case 'PUT':
            case 'PATCH': {
            return [
                'sku' => 'required|unique:products|max:255',
                'name' => 'required|max:255',
                'desc' => 'required|max:255',
                'price' => 'required|numeric',
                'promotion_price' => 'required|numeric',
                'picture_url' => 'required|max:255',
                'link_url' => 'required|max:255',
                'notes' => 'required|max:255',
            ];
            }
            case 'GET':
            case 'DELETE':
            default: {
                return [];
            }
        }
    }

    public function messages()
    {
        return [
            // Validation messages
        ];
    }
}
