<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use Maatwebsite\Excel\Facades\Excel;

class ProductsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'show']]);
    }

	public function index(ProductRequest $request)
	{
        $where = [];
        if ($skuStr = $request->sku) {
            //$skuArr = explode(',', $skuStr);
            $where['sku']= $skuStr;
        }
        if ($name = $request->name) {
            $where[] = ['name', 'like', '%' . $name . '%'];
        }
		$products = Product::withCount('emails')->where($where)->recent()->paginate(50);
        $products->appends(['name' => $name, 'sku'=> $skuStr]);

		return view('products.index', compact('products'));
	}

    public function show(Product $product)
    {
        return view('products.show', compact('product'));
    }

	public function create(Product $product)
	{
		return view('products.create_and_edit', compact('product'));
	}

	public function store(ProductRequest $request)
	{
        if ($request->excel) {
            return $this->import($request);
        }
		$product = Product::create($request->all());
		return redirect()->route('products.show', $product->id)->with('message', 'Created successfully.');
	}

	public function edit(Product $product)
	{
        $this->authorize('update', $product);
		return view('products.create_and_edit', compact('product'));
	}

	public function update(ProductRequest $request, Product $product)
	{
		$this->authorize('update', $product);
		$product->update($request->all());

		return redirect()->route('products.show', $product->id)->with('message', 'Updated successfully.');
	}

	public function destroy(Product $product)
	{
		$this->authorize('destroy', $product);
		$product->delete();

		return redirect()->route('products.index')->with('message', 'Deleted successfully.');
	}

    private function import(Request $request)
    {
        $this->validate($request, ['file' => 'required']);

        try {
            Excel::load($request->file('file'), function ($reader) {
                foreach ($reader->toArray() as $row) {
                    Product::firstOrCreate($row);
                }
            });
            \Session::flash('success', 'Products uploaded successfully.');
        } catch (\Exception $e) {
            \Session::flash('error', $e->getMessage());
        }
        return redirect(route('products.index'));
    }
}