<?php

namespace App\Http\Controllers;

use App\Models\SendingEmail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\SendingEmailRequest;

class SendingEmailsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'show']]);
    }

	public function index(Request $request)
	{
        $where = [];
        if (isset($request->send_status)) {
            $where['send_status'] = $request->send_status;
        }
		$sending_emails = SendingEmail::with(['member:id,name,email', 'template:id,name', 'products'])->where($where)->recent()->paginate();
        $sending_emails->appends(['send_status'=> $request->status]);
        $status_map = $this->getStatusName();
		return view('sending_emails.index', compact('sending_emails', 'status_map'));
	}

    public function show(SendingEmail $sending_email)
    {
        $sending_email->load('template:id,name', 'member:id,name,email', 'products');
        dd($sending_email->toArray());
        return view('sending_emails.show', compact('sending_email'));
    }

	public function create(SendingEmail $sending_email)
	{
		return view('sending_emails.create_and_edit', compact('sending_email'));
	}

	public function store(SendingEmailRequest $request)
	{
		$sending_email = SendingEmail::create($request->all());
		return redirect()->route('sending_emails.show', $sending_email->id)->with('message', 'Created successfully.');
	}

	public function edit(SendingEmail $sending_email)
	{
        $this->authorize('update', $sending_email);
        $sending_email->load('template:id,name', 'member:id,name,email', 'products');
		return view('sending_emails.create_and_edit', compact('sending_email'));
	}

	public function update(SendingEmailRequest $request, SendingEmail $sending_email)
	{
		$this->authorize('update', $sending_email);
		$sending_email->update($request->all());

		return redirect()->route('sending_emails.show', $sending_email->id)->with('message', 'Updated successfully.');
	}

	public function destroy(SendingEmail $sending_email)
	{
		$this->authorize('destroy', $sending_email);
		$sending_email->delete();

		return redirect()->route('sending_emails.index')->with('message', 'Deleted successfully.');
	}

    public function getStatusName($status = null)
    {
        $map = [
            0 => '未开始',
            1 => '已完成',
            2 => '投递失败',
            3 => '异常邮箱',
        ];

        if (is_null($status)) {
            return $map;
        }

        return $map[$status];
    }
}