<?php

namespace App\Http\Controllers;

use App\Models\CrawlUserEmail;
use App\Models\EmailTemplate;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CrawlUserEmailRequest;

class CrawlUserEmailsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'show']]);
    }

	public function index(CrawlUserEmailRequest $request)
	{
        $where = [];
        if ($skuStr = $request->sku) {
            //$skuArr = explode(',', $skuStr);
            $where['sku']= $skuStr;
        }
		$crawl_user_emails = CrawlUserEmail::where($where)->recent()->paginate();
        $crawl_user_emails->appends(['sku' => $skuStr]);
		return view('crawl_user_emails.index', compact('crawl_user_emails'));
	}

    public function show(CrawlUserEmail $crawl_user_email)
    {
        return view('crawl_user_emails.show', compact('crawl_user_email'));
    }

	public function create(CrawlUserEmailRequest $request)
	{
        $crawl_product_ids = '';
        if ($id = $request->get('id')) {
            $crawl_user_email = CrawlUserEmail::find($id);
            $crawl_product_ids = $crawl_user_email['sku'];
        }
        $productModel = new Product();
        $products = $productModel->limit(20)->get();
        $products = $products->map(function ($item) {
            $item['text'] = $item['name'];
            return $item;
        });

        $templates = (new EmailTemplate())->get();
		return view('crawl_user_emails.create_and_edit', compact('templates', 'products', 'crawl_product_ids'));
	}

	public function store(CrawlUserEmailRequest $request)
	{
		$crawl_user_email = CrawlUserEmail::create($request->all());
		return redirect()->route('crawl_user_emails.show', $crawl_user_email->id)->with('message', 'Created successfully.');
	}

	public function edit(CrawlUserEmail $crawl_user_email)
	{
        $this->authorize('update', $crawl_user_email);
		return view('crawl_user_emails.create_and_edit', compact('crawl_user_email'));
	}

	public function update(CrawlUserEmailRequest $request, CrawlUserEmail $crawl_user_email)
	{
		$this->authorize('update', $crawl_user_email);
		$crawl_user_email->update($request->all());

		return redirect()->route('crawl_user_emails.show', $crawl_user_email->id)->with('message', 'Updated successfully.');
	}

	public function destroy(CrawlUserEmail $crawl_user_email)
	{
		$this->authorize('destroy', $crawl_user_email);
		$crawl_user_email->delete();

		return redirect()->route('crawl_user_emails.index')->with('message', 'Deleted successfully.');
	}
}