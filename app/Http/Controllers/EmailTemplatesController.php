<?php

namespace App\Http\Controllers;

use App\Models\EmailTemplate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\EmailTemplateRequest;

class EmailTemplatesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'show']]);
    }

    public function index()
    {
        $email_templates = EmailTemplate::with('type')->withCount('emails')->paginate();

        return view('email_templates.index', compact('email_templates'));
    }

    public function show(EmailTemplate $email_template)
    {
        $result = EmailTemplate::with('type')->withCount('emails')->find($email_template);
        dd($result->toArray());
        return view('email_templates.show', compact('email_template'));
    }

    public function create(EmailTemplate $email_template)
    {
        return view('email_templates.create_and_edit', compact('email_template'));
    }

    public function store(EmailTemplateRequest $request)
    {
        $email_template = EmailTemplate::create($request->all());

        return redirect()->route('email_templates.show', $email_template->id)->with('message', 'Created successfully.');
    }

    public function edit(EmailTemplate $email_template)
    {
        $this->authorize('update', $email_template);

        return view('email_templates.create_and_edit', compact('email_template'));
    }

    public function update(EmailTemplateRequest $request, EmailTemplate $email_template)
    {
        $this->authorize('update', $email_template);
        $email_template->update($request->all());

        return redirect()->route('email_templates.show', $email_template->id)->with('message', 'Updated successfully.');
    }

    public function destroy(EmailTemplate $email_template)
    {
        $this->authorize('destroy', $email_template);
        $email_template->delete();

        return redirect()->route('email_templates.index')->with('message', 'Deleted successfully.');
    }
}