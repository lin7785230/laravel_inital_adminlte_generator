<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;

class FailedJobsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $where = [];
        if ($request->queue) {
            $where['queue'] = $request->queue;
        }
        $jobs = $this->getTable()->where($where)->paginate();

        return view('failed_jobs.index', compact('jobs'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $job = $this->failJobsServer()->find($id);

        return view('failed_jobs.show', compact('job'));
    }

    public function redo(Request $request)
    {
        $name = 'all';
        if ($request->id) {
            $name = $request->id;
        }
        Artisan::call('queue:retry', ['id' => $name]);

        return response()->redirectToRoute('failed_jobs.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Artisan::call('queue:forget', ['id' => $id]);

        return $this->responseJson();
    }

    public function flush()
    {
        Artisan::call('queue:flush');

        return response()->redirectToRoute('failed_jobs.index');
    }

    private function failJobsServer()
    {
        return app('queue.failer');
    }

    /**
     * Get a new query builder instance for the table.
     *
     * @return \Illuminate\Database\Query\Builder
     */
    protected function getTable()
    {
        return DB::table('failed_jobs');
    }
}
