<?php

namespace App\Http\Controllers;


use App\Models\CrawlUserEmail;
use App\Models\EmailTemplate;
use App\Models\Member;
use App\Models\Product;
use App\Models\SendingEmail;
use App\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class EmailSendController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'show', 'mailgun_hook', 'mailgunHook']]);
    }

    /**
     * 邮件发送页面
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
	public function create()
	{
        $memberModel = new Member();
        $members = $memberModel->limit(20)->get();
        $members = $members->map(function ($item) {
            $item['text'] = $item['name'];
            return $item;
        });

        $productModel = new Product();
        $products = $productModel->limit(20)->get();
        $products = $products->map(function ($item) {
            $item['text'] = $item['name'];
            return $item;
        });
        $templates = (new EmailTemplate())->get();
        $suggest_plant_at = emailLastPlantAt();

		return view('email_send.create', compact('members', 'products', 'templates', 'suggest_plant_at'));
	}

    /**
     * 邮件发送动作
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
	public function store(Request $request)
	{
        $memberIds = $request->member_ids;
        if ($request->crawl_product_ids) {
            $memberIds = $this->getMemberIdFromCrawlUser($request->crawl_product_ids);
        }

        $templateId = $request->template_id;
        $productIds = $request->product_ids;
        if ($request->plant_at) {
            $when = Carbon::parse($request->plant_at);
            $recentPlantAt = emailLastPlantAt();
            //发送时间不能小于当前最晚一封邮件发送的时间
            if ($when->lt($recentPlantAt)) {
                $when = $recentPlantAt;
            }
        };
        if (!$memberIds || !$templateId || !$productIds) {
            return $this->responseJsonFail('参数错误!');
        }
        $productIds = Product::whereIn('id', $productIds)->get()->pluck('id');
        if (empty($productIds)) {
            return $this->responseJsonFail('商品数据不存在!');
        }
        $TemplateModel = new EmailTemplate();
        $template = $TemplateModel->select('class_name')->find($templateId);
        if (empty($template)) {
            return $this->responseJsonFail('邮件模板不存在!');
        }
        $memberModel = new Member();
        $members = $memberModel->whereIn('id', $memberIds)->get()->toArray();
        if (empty($members)) {
            return $this->responseJsonFail('会员数据不存在!');
        }

        $plantAt = $when ?? now();
        foreach ($members as $member) {
            // 新增邮件
            $insertData = [
                'email_template_id' => $templateId,
                'member_id' => $member['id'],
                'status' => 1,
                'plant_at' => $plantAt,
            ];
            $sendingEmail = SendingEmail::create($insertData);
            if (!$sendingEmail) {
                logger('插入邮件发送记录数据失败');
                continue;
            }
            // 更新商品关系
            $sendingEmail->products()->attach($productIds);
            $sendingEmail->product_ids = $productIds;
            // 每个邮件之间间隔37s，保证一个小时内不会发送超过100封
            if (cache(emailLimitCacheKey($plantAt)) >= 300) {
                // 一天发送超过300 则第二天再发送
                $plantAt = Carbon::instance($plantAt)->addDay()->hour(0)->minute(0);
            } else {
                $plantAt = Carbon::instance($plantAt)->addSeconds(37);
            }
            // 发送邮件
            $class = $template['class_name'];
            $mailClass = new $class($sendingEmail);
            $this->mailSendQueue($mailClass, $sendingEmail['plant_at']);
        }

        return $this->responseJson();
	}

    public function mailSendQueue($mailClass, $plantAt)
    {
        // 计数增加
        $key = emailLimitCacheKey($plantAt);
        if (!cache()->has($key)) {
            cache([$key, 0], Carbon::instance($plantAt)->addWeek());
        }
        cache()->increment(emailLimitCacheKey($plantAt));
        Mail::queue($mailClass);
	}

    /**
     * 重新发送邮件
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function retry(Request $request)
    {
        if (!$id = $request->id) {
            return $this->responseJsonFail('参数错误');
        }

        $sendingEmail = SendingEmail::find($id);
        $templateId = $sendingEmail['email_template_id'];
        $TemplateModel = new EmailTemplate();
        $template = $TemplateModel->select('class_name')->find($templateId);
        if (empty($template)) {
            return $this->responseJsonFail('邮件模板不存在!');
        }

        $sendingEmail['plant_at'] = emailLastPlantAt();
        $sendingEmail['send_status'] = 0;
        $sendingEmail->save();

        $class = $template['class_name'];
        $mailClass = new $class($sendingEmail);
        $this->mailSendQueue($mailClass, $sendingEmail['plant_at']);
        return $this->responseJson();
	}

    /**
     * 全部重新发送
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function retryAll(Request $request)
    {
        $templates = EmailTemplate::all()->keyBy('id');
        SendingEmail::where('send_status', 2)->recent()->chunk(100, function ($failEmails) use ($templates) {
            $memberIds = $failEmails->pluck('member_id');
            $memberMap = Member::whereIn('id', $memberIds)->get(['id', 'email'])->keyBy('id');
            $plantAt = emailLastPlantAt();
            foreach ($failEmails as $sendingEmail) {
                //跳过非法的邮箱
                if (skipSendEmail($memberMap[$sendingEmail['member_id']])) {
                    $sendingEmail['send_status'] = 3;
                    $sendingEmail->save();
                    continue;
                }
                $template = $templates[$sendingEmail['email_template_id']];
                // 每个邮件之间间隔37s，保证一个小时内不会发送超过100封
                if (cache(emailLimitCacheKey($plantAt)) >= 300) {
                    // 一天发送超过300 则第二天再发送
                    $plantAt = Carbon::instance($plantAt)->addDay()->hour(0)->minute(0);
                } else {
                    $plantAt = Carbon::instance($plantAt)->addSeconds(37);
                }
                // 发送邮件
                $sendingEmail['plant_at'] = $plantAt;
                $sendingEmail['send_status'] = 0;
                $result = $sendingEmail->save();
                if ($result) {
                    $class = $template['class_name'];
                    $mailClass = new $class($sendingEmail);
                    $this->mailSendQueue($mailClass, $sendingEmail['plant_at']);
                }
            }
        });


        return $this->responseJson();
	}

    /**
     * select2 一定要有 title和id
     * 会员查找动作
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function searchMember(Request $request)
    {
        $keyWord = $request->q;
        $memberIds = $request->member_ids;
        $memberModel = new Member();
        $memberModel = $memberModel->select(['*', 'name as text']);
        if ($keyWord) {
            $members = $memberModel->where('name', 'like', '%' . $keyWord . '%')->orWhere('email', 'like', '%' . $keyWord . '%')->paginate();
        } else if($memberIds) {
            $members = $memberModel->whereIn('id', explode(',', $memberIds))->get();
        } else {
            $members = $memberModel->paginate();
        }

        return response()->json($members);
	}

    /**
     * 查找货品
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function searchProduct(Request $request)
    {
        $keyWord = $request->q;
        $productIds = $request->product_ids;
        $productModel = new Product();
        $productModel = $productModel->select(['*', 'name as text']);
        if ($keyWord) {
            $products = $productModel->where('name', 'like', '%' . $keyWord . '%')->orWhere('sku', 'like', '%' . $keyWord . '%')->paginate();
        } else if($productIds) {
            $products = $productModel->whereIn('id', explode(',', $productIds))->get();
        } else {
            $products = $productModel->paginate();
        }

        return response()->json($products);
    }

    /**
     * 查看发送模板样式
     * @param Request $request
     */
    public function checkStyle(Request $request)
    {
        $productIds = (array)$request->product_ids;
        $templateId = $request->template_id;
        if (!$templateId || !$productIds) {
            return $this->responseJsonFail('参数错误!');
        }

        $TemplateModel = new EmailTemplate();
        $template = $TemplateModel->find($templateId);
        $memberModel = new Member();
        $member = $memberModel->first();

        $sendingEmail = new SendingEmail();
        $sendingEmail['member_id'] = $member->id;
        $sendingEmail['product_ids'] = $productIds;
        $class = $template['class_name'];

        return new $class($sendingEmail);
    }

    /**
     * mailGun Webhook
     * @param Request $request
     */
    public function mailgunHook(Request $request)
    {
        logger('webhook info', $request->all());
        return response();
    }

    // https://app.mailgun.com/app/webhooks
    public function mailDelivered(Request $request)
    {
        logger('email_send!', $request->all());
    }

    public function mailDropped(Request $request)
    {
        logger('email_drop', $request->all());
    }

    public function getMemberIdFromCrawlUser($asinStr)
    {
        $asin = explode(',', $asinStr);
        $memberIds = [];
        $crawlUsers = CrawlUserEmail::whereIn('sku', $asin)->select(['user_id', 'user_name', 'email'])->get();
        $memberModel = new Member();
        foreach ($crawlUsers as $crawlUser) {
            if (skipSendEmail($crawlUser['email'])) {
                continue;
            }
            $data = [
                'name'  => $crawlUser['user_name'],
                'email' => $crawlUser['email'],
            ];
            $member = $memberModel->firstOrCreate($data);
            $memberIds[] = $member['id'];
        }
        $memberIds = array_unique($memberIds);
        return $memberIds;
    }

}