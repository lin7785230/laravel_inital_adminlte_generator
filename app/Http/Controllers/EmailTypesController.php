<?php

namespace App\Http\Controllers;

use App\Models\EmailType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\EmailTypeRequest;

class EmailTypesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'show']]);
    }

	public function index()
	{
		$email_types = EmailType::paginate();
		return view('email_types.index', compact('email_types'));
	}

    public function show(EmailType $email_type)
    {
        return view('email_types.show', compact('email_type'));
    }

	public function create(EmailType $email_type)
	{
		return view('email_types.create_and_edit', compact('email_type'));
	}

	public function store(EmailTypeRequest $request)
	{
		$email_type = EmailType::create($request->all());
		return redirect()->route('email_types.show', $email_type->id)->with('message', 'Created successfully.');
	}

	public function edit(EmailType $email_type)
	{
        $this->authorize('update', $email_type);
		return view('email_types.create_and_edit', compact('email_type'));
	}

	public function update(EmailTypeRequest $request, EmailType $email_type)
	{
		$this->authorize('update', $email_type);
		$email_type->update($request->all());

		return redirect()->route('email_types.show', $email_type->id)->with('message', 'Updated successfully.');
	}

	public function destroy(EmailType $email_type)
	{
		$this->authorize('destroy', $email_type);
		$email_type->delete();

		return redirect()->route('email_types.index')->with('message', 'Deleted successfully.');
	}
}