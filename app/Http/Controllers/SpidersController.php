<?php

namespace App\Http\Controllers;

use App\Jobs\CrawlAmazonReviewerJob;
use App\Models\Spider;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\SpiderRequest;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class SpidersController extends Controller
{
    const CACHE_NAME = 'spider_cookies';

    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'show']]);
    }

	public function index(SpiderRequest $request)
	{
        $where = [];
        if ($name = $request->name) {
            $where[] = [
                'name', 'like', '%'. $name . '%',
            ];
        }

        if (isset($request->status)) {
            $where['status'] = $request->status;
        }
		$spiders = Spider::where($where)->recent()->paginate();
        $spiders->appends(['name' => $name, 'status'=> $request->status]);

        $status_map = $this->getStatusName();
		return view('spiders.index', compact('spiders', 'status_map'));
	}

    public function show(Spider $spider)
    {
        $spider['status_name'] = $this->getStatusName($spider['status']);
        return view('spiders.show', compact('spider'));
    }

	public function create(Spider $spider)
	{
        $status_map = $this->getStatusName();
		return view('spiders.create_and_edit', compact('spider', 'status_map'));
	}

	// scrapy crawl amazon_reviewer -o B075FLBJV7.json -a product_id=B075FLBJV7 -a max_page_no=50
    public function store(SpiderRequest $request)
	{
        $this->checkCookies();
        $spiderName = $request->input('spider_name', 'amazon_reviewer');
        $sku = $request->input('sku');
        $orderBy = $request->input('sort_by', 'helpful');
        $filterStar = $request->input('filter_by_star', 'all_stars');

        //每个脚本最多执行的翻页数量
        $pageSize = $request->input('page_size', 50);
        $breakMinutes = $request->input('break_minutes', 25);
        $maxPageNo = $request->input('max_page_no', $pageSize);
        $minPageNo = $request->input('min_page_no', 1);
        $plantAt = $request->input('plant_at');
        if ($plantAt) {
            $plantAt = date('Y-m-d H:i:s', strtotime($plantAt));
        }

        // 页码判断
        if ($minPageNo > $maxPageNo) {
            return redirect()->route('spiders.create')->withErrors('结束页码不能大于开始页码！');
        }

        // 向上取整
        $pageTotal = ceil(($maxPageNo - $minPageNo + 1) / $pageSize);
        while ($pageTotal >= 1) {
            $startPage = $minPageNo;
            $endPage = $startPage + $pageSize - 1;
            if ($endPage > $maxPageNo) {
                $endPage = $maxPageNo;
            }
            // 组织 spider 命令
            $fileName = $request->name . "{$startPage}-{$endPage}";
            $logFileName = $this->formatLogFileName($fileName);
            $script = sprintf('scrapy crawl %s -s LOG_FILE=logs/%s -a product_id=%s -a min_page_no=%s -a max_page_no=%s -a filterByStar=%s -a sortBy=%s',
                              $spiderName,
                              $logFileName,
                              $sku,
                              $startPage,
                              $endPage,
                              $filterStar,
                              $orderBy
            );
            // 新增spider 命令
            $data = [
                'name'     => $fileName,
                'content'  => $script,
                'status'  => 0,
                'plant_at' => $plantAt,
            ];
            $spider = Spider::create($data);
            // 添加任务
            $this->spiderJob($spider);
            // 每个脚本之间执行的间隔时间，目前为半小时
            $restTime = $breakMinutes + random_int(0, 10);
            $plantAt = Carbon::createFromFormat('Y-m-d H:i:s', $plantAt)->addMinute($restTime);
            $minPageNo += $pageSize;
            $pageTotal--;
        }

		return redirect()->route('spiders.index')->with('message', 'Created successfully.');
	}

	public function edit(Spider $spider)
	{
        $this->authorize('update', $spider);

        // 正则表达式解析 content 参数
        /*$spider['content'];
        $re = '/scrapy crawl (\w+) -o (\w+).json -a product_id=(\w+) -a max_page_no=(\d+) -a filterByStar=(\w+) -a sortBy=(\w+)/';
        preg_match($re, $spider['content'], $matches);

        $spider['spider_name'] = $matches[1] ?? 'amazon_reviewer';
        $spider['sku'] = $matches[3] ?? '';
        $spider['max_page_no'] = $matches[4] ?? 50;
        $spider['filter_by_star'] = $matches[5] ?? 'all_stars';
        $spider['recent'] = $matches[6] ?? 'recent';*/
        $status_map = $this->getStatusName();
		return view('spiders.create_and_edit', compact('spider', 'status_map'));
	}

	public function update(SpiderRequest $request, Spider $spider)
	{
		$this->authorize('update', $spider);
        $data = $request->all();
        $originStatus = $spider['status'];
        $data['plant_at'] = date('Y-m-d H:i:s', strtotime($data['plant_at']));
		$spider->update($data);
		// 状态改变，并且未未开始状态
        if (0 == $data['status'] && $originStatus != $data['status']) {
            $this->spiderJob($spider);
        }
		return redirect()->route('spiders.show', $spider->id)->with('message', 'Updated successfully.');
	}

	public function destroy(Spider $spider)
	{
		$this->authorize('destroy', $spider);
		$spider->delete();

		return redirect()->route('spiders.index')->with('message', 'Deleted successfully.');
	}

    private function spiderJob(Spider $spider)
    {
        return CrawlAmazonReviewerJob::dispatch($spider)->onQueue('spiders')->delay($spider['plant_at']);
	}

    public function cookies()
    {
        $cookies_content = cache(self::CACHE_NAME);
        $last_modify = Storage::disk('spider')->lastModified(config('amazon_mail.spider_cookies_file'));
        $last_modify = Carbon::createFromTimestamp($last_modify);
        return view('spiders.cookies', compact('cookies_content', 'last_modify'));
	}

    public function storeCookies(Request $request)
    {
        $cookies_content= $request->cookies_content;
        // 缓存
        cache([self::CACHE_NAME => $cookies_content], Carbon::now()->addDays(2));
        // 保存为文件
        Storage::disk('spider')->put(config('amazon_mail.spider_cookies_file'), $cookies_content, 'public');
        return response()->redirectToRoute('spider_cookies');
	}

    private function checkCookies()
    {
        if (empty(cache(self::CACHE_NAME))) {
            return response()->redirectToRoute('spider_cookies')->withErrors(['更新登录cookies信息']);
        }

        return true;
	}

    public function getStatusName($status = null)
    {
        $map = [
            0 => '未开始',
            1 => '已完成',
            2 => '进行中',
            3 => 'Detected',
        ];

        if (is_null($status)) {
            return $map;
        }

        return $map[$status];
	}

    public function spider_log(Request $request)
    {
        if (!$fileName = $request->file_name) {
            return '参数错误';
        }
        $file = config('amazon_mail.spider_root') . 'logs/' . $this->formatLogFileName($fileName);
        if (!file_exists($file)) {
            return '暂无日志';
        }

        return shell_exec('tail -n 200 '. $file);
    }

    private function formatLogFileName($fileName)
    {
        return Str::kebab($fileName) . '.log';
	}
}