<?php

namespace App\Http\Controllers;

use App\Models\SpiderAccount;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\SpiderAccountRequest;

class SpiderAccountsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'show']]);
    }

	public function index()
	{
		$spider_accounts = SpiderAccount::paginate();
		return view('spider_accounts.index', compact('spider_accounts'));
	}

    public function show(SpiderAccount $spider_account)
    {
        return view('spider_accounts.show', compact('spider_account'));
    }

	public function create(SpiderAccount $spider_account)
	{
		return view('spider_accounts.create_and_edit', compact('spider_account'));
	}

	public function store(SpiderAccountRequest $request)
	{
		$spider_account = SpiderAccount::create($request->all());
		return redirect()->route('spider_accounts.show', $spider_account->id)->with('message', 'Created successfully.');
	}

	public function edit(SpiderAccount $spider_account)
	{
        $this->authorize('update', $spider_account);
		return view('spider_accounts.create_and_edit', compact('spider_account'));
	}

	public function update(SpiderAccountRequest $request, SpiderAccount $spider_account)
	{
		$this->authorize('update', $spider_account);
		$spider_account->update($request->all());

		return redirect()->route('spider_accounts.show', $spider_account->id)->with('message', 'Updated successfully.');
	}

	public function destroy(SpiderAccount $spider_account)
	{
		$this->authorize('destroy', $spider_account);
		$spider_account->delete();

		return redirect()->route('spider_accounts.index')->with('message', 'Deleted successfully.');
	}
}