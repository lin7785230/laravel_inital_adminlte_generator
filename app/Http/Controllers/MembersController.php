<?php

namespace App\Http\Controllers;

use App\Models\Member;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\MemberRequest;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;

class MembersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'show']]);
    }

	public function index(MemberRequest $request)
	{
        $where = [];
        if ($email = $request->email) {
            $where['email']= $email;
        }
        if ($name = $request->name) {
            $where['name'] = $name;
        }

		$members = Member::where($where)->recent()->paginate(100);
        $members->appends(['name' => $name, 'email'=> $email]);

		return view('members.index', compact('members'));
	}

    public function show(Member $member)
    {
        return view('members.show', compact('member'));
    }

	public function create(Member $member)
	{
		return view('members.create_and_edit', compact('member'));
	}

	public function store(MemberRequest $request)
	{
        if ($request->excel) {
            return $this->import($request);
        }
		$member = Member::create($request->all());
		return redirect()->route('members.show', $member->id)->with('message', 'Created successfully.');
	}

	public function edit(Member $member)
	{
        $this->authorize('update', $member);
		return view('members.create_and_edit', compact('member'));
	}

	public function update(MemberRequest $request, Member $member)
	{
		$this->authorize('update', $member);
		$member->update($request->all());

		return redirect()->route('members.show', $member->id)->with('message', 'Updated successfully.');
	}

	public function destroy(Member $member)
	{
		$this->authorize('destroy', $member);
		$member->delete();

		return redirect()->route('members.index')->with('message', 'Deleted successfully.');
	}

    private function import(Request $request)
    {
        $this->validate($request, ['file' => 'required']);

        try {
            Excel::load($request->file('file'), function ($reader) {
                foreach ($reader->toArray() as $row) {
                    Member::firstOrCreate($row);
                }
            });
            \Session::flash('success', 'Members uploaded successfully.');
        } catch (\Exception $e) {
            \Session::flash('error', $e->getMessage());
        }
        return redirect(route('members.index'));
    }
}