<?php


namespace App\Http\Controllers;


use App\DataTables\UsersDataTable;
use App\Models\User;
use Yajra\DataTables\Facades\DataTables;

class UsersController extends Controller
{
    public function index(UsersDataTable $dataTable)
    {
        return $dataTable->render('users.index');
    }

    public function list()
    {
        return Datatables::of(User::query())->make(true);
    }
}