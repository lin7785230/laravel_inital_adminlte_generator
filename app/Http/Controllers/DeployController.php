<?php


namespace App\Http\Controllers;
use App\Jobs\{
    GitJob
};

/**
 * webhook 自动部署
 * Class DeployController
 * @package App\Http\Controllers
 */
class DeployController extends Controller
{
    public function index()
    {
        $this->checkToken();
        $this->execCall();
    }

    protected function execCall()
    {
        $branch = config('gitlab_deploy.branch');
        $afterScript = '';
        if (empty($branch) || request()->ref == $branch) {
            GitJob::dispatch();
        } else {
            logger('=== ERROR: Pushed branch does not match BRANCH ===');
        }
    }

    protected function checkToken()
    {
        $checkToken = config('gitlab_deploy.token');
        $token = false;
        // retrieve the token
        if (isset($_SERVER["HTTP_X_GITLAB_TOKEN"])) {
            $token = $_SERVER["HTTP_X_GITLAB_TOKEN"];
        } elseif (request()->get('token')) {
            $token = request()->get('token');
        }
        // Check for a GitLab token
        if (!empty($checkToken) && isset($_SERVER["HTTP_X_GITLAB_TOKEN"]) && $token !== $checkToken) {
            $this->fail("X-GitLab-Token does not match TOKEN");
            // Check for a $_GET token
        } elseif (!empty($checkToken) && isset($token) && $token !== $checkToken) {
            $this->fail("\$_GET[\"token\"] does not match TOKEN");
            // if none of the above match, but a token exists, exit
        } elseif (!empty($checkToken) && !isset($_SERVER["HTTP_X_GITLAB_TOKEN"]) && !isset($_GET["token"])) {
            $this->fail("No token detected");
        }
    }

    public function success()
    {
        ob_start();
        header("HTTP/1.1 200 OK");
        header("Connection: close");
        header("Content-Length: " . ob_get_length());
        ob_end_flush();
        ob_flush();
        flush();
    }

    public function fail($reason)
    {
        // explain why
        logger("=== ERROR: " . $reason . " ===");
        logger('*** ACCESS DENIED ***');
        // forbid
        header("HTTP/1.0 403 Forbidden");
        exit;
    }
}