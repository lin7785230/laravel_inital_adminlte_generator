<?php

namespace App\Mail;

use App\Models\Product;
use Exception;
use App\Models\Member;
use App\Models\SendingEmail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;


class ProductRecommend extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * 商品信息
     * @var
     */
    public  $products;

    /**
     * 设置队列名称
     * @var
     */
    public $queue;

    /**
     * 设置延迟时间
     * @var
     */
    public $delay;

    /**
     * @var SendingEmail
     */
    public $email;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(SendingEmail $email)
    {
        $this->email = $email;
        $this->queue = 'emails';

        $this->subject('Amazon Product Recommend');
        if ($email['plant_at'] && now()->lt($email['plant_at'])) {
            $this->delay = $email['plant_at'];
        }
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->attachHeaders();
        $this->products = $this->getProduct();
        $member = $this->getMember();
        $to = $member['email'];
        return $this->from('lin7785230@gmail.com', 'Amazon Product Recommend')->to($to)->view('mails.before_sales.amazon')->with('memberName', $member['name']);
    }

    /**
     * 当发送失败时，处理返回内容
     *
     * @param Exception $exception
     */
    public function failed(Exception $exception)
    {
        $this->email->send_status = 2;
        $this->email->save();
        logger('任务执行失败 failed', [$exception->getMessage(), $exception->getFile(), $exception->getLine()]);
    }

    /**
     * 增加message_id，webhook 回调时会将该信息放在头部
     */
    public function attachHeaders()
    {
        $data = json_encode(['message_id' => $this->email->id]);
        $this->withSwiftMessage(function ($message) use ($data) {
            $message->getHeaders()
                ->addTextHeader('X-Mailgun-Variables', $data);
        });
    }

    /**
     * 发送的会员信息
     * @return array|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|
     */
    public function getMember()
    {
        $memberId = $this->email->member_id;
        $member = Member::find($memberId);

        return $member;
    }

    public function getProduct()
    {
        // 兼容测试使用
        $productIds = object_get($this->email, 'product_ids', '');
        if (empty($productIds)) {
            $productIds = DB::table('email_product')->select('product_id')->where(['email_id' => $this->email->id])->get()->pluck('product_id')->toArray();
        }

        if (empty($productIds)) {
            return $this->defaultProduct();
        }

        return Product::whereIn('id', $productIds)->get()->toArray();
    }

    private function defaultProduct()
    {
        return [
            'name'            => '测试产品',
            'desc'            => '说一段非常长的话！！！！',
            'price'           => 200.00,
            'promotion_price' => 150.00,
            'picture_url'     => 'https://gss3.bdstatic.com/-Po3dSag_xI4khGkpoWK1HF6hhy/baike/w%3D268%3Bg%3D0/sign=dd5630acbb389b5038ffe754bd0e82e0/b21bb051f8198618ddfdc53648ed2e738ad4e6ce.jpg',
            'link_url'        => 'www.amazon.cn',
        ];
    }
}
