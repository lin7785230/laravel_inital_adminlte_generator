<?php

namespace App\Mail;


class ProductRecommendV2 extends ProductRecommend
{
    public $products;
    public $mailView = 'mails.before_sales.amazon_v2';
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->products = $this->getProduct();
        $member = $this->getMember();
        $to = $member['email'];
        return $this->from('lin7785230@gmail.com', 'Amazon Product Recommend')->to($to)->view($this->mailView)->with('memberName', $member['name'])->with('memberEmail', $member['email']);
    }
}
