<?php

namespace App\Jobs;

use App\Models\Spider;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Artisan;

class CrawlAmazonReviewerJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $spider;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Spider $spider)
    {
        $this->spider = $spider;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // 查找未开始的
        $spider = Spider::where(['status' => 0])->find($this->spider['id']);
        if ($spider) {
            # 组装新参数
            $script = $spider['content'] . ' -a spider_id='. $this->spider['id'];
            // 进行中
            $spider['status'] = 2;
            $spider->save();
            chdir(config('amazon_mail.spider_root'));
            $prefix = 'PATH=$PATH:/usr/local/bin;export PATH;';
            $cmd = $prefix . $script;
            exec($cmd, $output);
            // 执行完成
            $spider = Spider::where(['status' => 2])->find($this->spider['id']);
            if ($spider) {
                $spider['status'] = 1;
                $spider->save();
            }
        }
    }
}
