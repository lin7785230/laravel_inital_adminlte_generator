<?php

namespace App\Jobs;

use App\Models\Spider;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Artisan;

class CrawlAmazonReviewerEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->onQueue('spiders');
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // 查找未开始的
        $spider = Spider::where(['status' => 0])->first();
        if ($spider) {
            // 进行中
            $spider['status'] = 2;
            $spider->save();

            chdir(config('amazon_mail.spider_root'));
            $prefix = 'PATH=$PATH:/usr/local/bin;export PATH;';
            $cmd = $prefix.$spider['content'];
            exec($cmd);

            // 执行完成
            $spider['status'] = 1;
            $spider->save();
        }
    }
}
