<?php

namespace App\Models;

class CrawlUserEmail extends Model
{
    protected $fillable = ['sku', 'user_id', 'user_name', 'email'];
}
