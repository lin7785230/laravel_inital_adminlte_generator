<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class EmailType extends Model
{
    use SoftDeletes;

    protected $fillable = ['name'];

    protected $dates = ['deleted_at'];

    /**
     *
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function templates()
    {
        return $this->hasMany(\App\Models\EmailTemplate::class, 'type_id', 'id');
    }

}
