<?php

namespace App\Models;

class SpiderAccount extends Model
{
    protected $fillable = ['name', 'password', 'type', 'content', 'status'];
}
