<?php

namespace App\Models;

/**
 * App\Models\SendingEmail
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model ordered()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model recent()
 * @mixin \Eloquent
 */
class SendingEmail extends Model
{
    protected $fillable = ['email_template_id', 'member_id', 'send_status', 'send_at', 'plant_at'];

    protected $dates = ['send_at', 'plant_at'];

    /**
     * 收件人
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function receiver()
    {
        return $this->belongsTo(\App\Models\Member::class);
    }

    /**
     * member
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function member()
    {
        return $this->belongsTo(\App\Models\Member::class);
    }

    /**
     * template
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function template()
    {
        return $this->belongsTo(\App\Models\EmailTemplate::class, 'email_template_id');
    }
    

    /**
     * product
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function products()
    {
        return $this->belongsToMany(\App\Models\Product::class, 'email_product', 'email_id', 'product_id');
    }
}
