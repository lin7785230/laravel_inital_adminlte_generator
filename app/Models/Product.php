<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;

    public $dates = ['deleted_at'];

    protected $fillable = ['sku','name', 'desc', 'price', 'promotion_price', 'picture_url', 'link_url', 'notes'];

    /**
     * 邮件
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function emails()
    {
        return $this->belongsToMany(\App\Models\SendingEmail::class, 'email_product', 'product_id', 'email_id');
    }
}
