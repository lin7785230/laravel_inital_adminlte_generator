<?php

namespace App\Models;
use Illuminate\Database\Eloquent\{
    SoftDeletes
};

/**
 * App\Models\Member
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model ordered()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model recent()
 * @mixin \Eloquent
 */
class Member extends Model
{
    use SoftDeletes;

    protected $fillable = ['id', 'name', 'email'];

    protected $dates = ['deleted_at'];

    /**
     * 发送邮件列表
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function emails()
    {
        return $this->hasMany(\App\Models\SendingEmail::class, 'member_id', 'id');
    }

    /**
     * 标签 tags
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags()
    {
        return $this->belongsToMany(\App\Models\Tag::class, 'member_tag');
    }


}
