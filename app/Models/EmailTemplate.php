<?php

namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\EmailTemplate
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model ordered()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model recent()
 * @mixin \Eloquent
 */
class EmailTemplate extends Model
{
    use SoftDeletes;

    protected $fillable = ['id', 'name', 'content', 'type_id'];

    protected $dates = ['deleted_at'];

    /**
     * Mails
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function emails()
    {
        return $this->hasMany(\App\Models\SendingEmail::class, 'email_template_id');
    }

    /**
     * type 类型
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo(\App\Models\EmailType::class, 'type_id', 'id');
    }
    

    /**
     * 使用该模板发送邮件的会员信息
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function members()
    {
        return $this->belongsToMany(\App\Models\Member::class, 'sending_emails', 'email_template_id', 'member_id');
    }
}
