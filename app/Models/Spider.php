<?php

namespace App\Models;

class Spider extends Model
{
    protected $fillable = ['name', 'content', 'status', 'plant_at'];
    protected $dates = ['plant_at'];
}
