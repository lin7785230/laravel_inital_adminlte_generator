@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="col-md-10">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h1>
                        <i class="glyphicon glyphicon-align-justify"></i> EmailTemplate
                        <a class="btn btn-success pull-right" href="{{ route('email_templates.create') }}"><i
                                    class="glyphicon glyphicon-plus"></i> Create</a>
                    </h1>
                </div>

                <div class="panel-body">
                    @if($email_templates->count())
                        <table class="table table-condensed table-striped">
                            <thead>
                            <tr>
                                <th class="text-center">#</th>
                                <th>Name</th>
                                <th>TypeId</th>
                                <th>EmailSum</th>
                                <th>ClassName</th>
                                <th>CreatedAt</th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($email_templates as $email_template)
                                <tr>
                                    <td class="text-center"><strong>{{$email_template->id}}</strong></td>

                                    <td>{{$email_template->name}}</td>
                                    <td>{{$email_template->type_id .'|'.$email_template->type->name}}</td>
                                    <td>{{$email_template->emails_count}}</td>
                                    <td>{{$email_template->class_name}}</td>
                                    <td>{{$email_template->created_at}}</td>

                                    {{--<td class="text-right">
                                        <a class="btn btn-xs btn-primary"
                                           href="{{ route('email_templates.show', $email_template->id) }}">
                                            <i class="glyphicon glyphicon-eye-open"></i>
                                        </a>

                                        <a class="btn btn-xs btn-warning"
                                           href="{{ route('email_templates.edit', $email_template->id) }}">
                                            <i class="glyphicon glyphicon-edit"></i>
                                        </a>

                                        <form action="{{ route('email_templates.destroy', $email_template->id) }}"
                                              method="POST" style="display: inline;"
                                              onsubmit="return confirm('Delete? Are you sure?');">
                                            {{csrf_field()}}
                                            <input type="hidden" name="_method" value="DELETE">

                                            <button type="submit" class="btn btn-xs btn-danger"><i
                                                        class="glyphicon glyphicon-trash"></i></button>
                                        </form>
                                    </td>--}}
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {!! $email_templates->render() !!}
                    @else
                        <h3 class="text-center alert alert-info">Empty!</h3>
                    @endif
                </div>
            </div>
        </div>
    </div>

@endsection