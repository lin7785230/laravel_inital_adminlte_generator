@extends('layouts.admin')

@section('content')

    <div class="container">
        <div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h1>EmailTemplate / Show #{{ $email_template->id }}</h1>
                </div>

                <div class="panel-body">
                    <div class="well well-sm">
                        <div class="row">
                            <div class="col-md-6">
                                <a class="btn btn-link" href="{{ route('email_templates.index') }}"><i
                                            class="glyphicon glyphicon-backward"></i> Back</a>
                            </div>
                            <div class="col-md-6">
                                <a class="btn btn-sm btn-warning pull-right"
                                   href="{{ route('email_templates.edit', $email_template->id) }}">
                                    <i class="glyphicon glyphicon-edit"></i> Edit
                                </a>
                            </div>
                        </div>
                    </div>

                    <input type="hidden" id="template_id" value="{{ $email_template->id }}">
                    <label>Name</label>
                    <p>
                        {{ $email_template->name }}
                    </p> <label>ClassName</label>
                    <p>
                        {!! old('class_name', $email_template->class_name ) !!}
                    </p>
                    <label>模板</label>
                    <div id="template">

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@push('js')
<script>
  $(function(){
    loadTemplate();
  });

  function loadTemplate() {
    $.ajax({
      url: '/email_send/check_style',
      type: 'POST',
      data: {
        member_ids: '1',
        product_ids: '1',
        template_id: $('#template_id').val()
      },
      success: function (response) {
        $('#template').html(response)
      },
      dataType: 'html'
    });
  }
</script>
@endpush
