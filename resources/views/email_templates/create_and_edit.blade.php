@extends('layouts.admin')

@section('content')

<div class="container">
    <div class="col-md-10">
        <div class="panel panel-default">

            <div class="panel-heading">
                <h1>
                    <i class="glyphicon glyphicon-edit"></i> EmailTemplate /
                    @if($email_template->id)
                        Edit #{{$email_template->id}}
                    @else
                        Create
                    @endif
                </h1>
            </div>

            @include('common.error')

            <div class="panel-body">
                @if($email_template->id)
                    <form action="{{ route('email_templates.update', $email_template->id) }}" method="POST" accept-charset="UTF-8">
                        <input type="hidden" name="_method" value="PUT">
                @else
                    <form action="{{ route('email_templates.store') }}" method="POST" accept-charset="UTF-8">
                @endif

                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                	<label for="name-field">Name</label>
                	<input class="form-control" type="text" name="name" id="name-field" value="{{ old('name', $email_template->name ) }}" />
                </div>
                <div class="form-group">
                    <label for="type_id-field">Type</label>
                    <input class="form-control" type="text" name="type_id" id="type_id-field" value="{{ old('type_id', $email_template->type_id ) }}" />
                </div>
                <div class="form-group">
                    <label for="class_name-field">ClassName</label>
                    <input class="form-control" type="text" name="class_name" id="class_name-field" value="{{ old('class_name', $email_template->class_name ) }}" />
                </div>
                {!! we_css() !!}
                {!! we_js() !!}
                <div class="form-group">
                	<label for="content-field">Content</label>
                    {!! we_field('wangeditor', 'content', old('name', $email_template->content )) !!}
                    {!! we_config('wangeditor') !!}
                </div>
                    <div class="well well-sm">
                        <button type="submit" class="btn btn-primary">Save</button>
                        <a class="btn btn-success" target="_blank"
                           href="{{ route('email_send.check_style', ['template_id'=> $email_template->id , 'member_ids' => 1, 'product_ids' => 1]) }}">
                            查看样式
                        </a>
                        <a class="btn btn-link pull-right" href="{{ route('email_templates.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

