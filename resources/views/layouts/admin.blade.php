@extends('adminlte::page')

@section('title', config('app.name', 'Laravel'))

@section('content_header')
    @yield('content_header')
@stop

@section('content')
    @yield('content')
@stop

@push('admin_css')
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    @yield('css')
    @stack('css')
@endpush

@push('admin_js')
<script src="https://cdn.bootcss.com/lodash.js/4.17.4/lodash.min.js"></script>
<script src="https://cdn.bootcss.com/axios/0.17.1/axios.min.js"></script>
{{--<script src="{{ asset('js/app.js') }}"></script>--}}
@stack('js')
@endpush