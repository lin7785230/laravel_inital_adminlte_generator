@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="col-md-10">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1>
                    <i class="glyphicon glyphicon-align-justify"></i> Member
                    <a class="btn btn-success pull-right" href="{{ route('members.create') }}"><i class="glyphicon glyphicon-plus"></i> Create</a>
                    <button type="button" class="btn btn-success pull-right margin-r-5" onclick="send_mail()">发送邮件</button>
                </h1>
                @component('_partial.import', ['template_url' => '/excel/members_template.xlsx', 'action' => route('members.store', ['excel'=>1])])
                @endcomponent
            </div>
            <div class="panel-body">
                <form class="form-inline" action="{{route('members.index')}}">
                    <div class="form-group">
                        <label for="search-name">Name</label>
                        <input type="text" class="form-control" name="name" id="search-name" value="{{ request('name') }}" placeholder="name">
                    </div>
                    <div class="form-group">
                        <label for="search-email">Email</label>
                        <input type="text" class="form-control" name="email" id="search-email" value="{{ request('email') }}" placeholder="email">
                    </div>
                    <button type="submit" class="btn btn-primary pull-right"><i class="glyphicon glyphicon-search"></i> Search</button>
                </form>
                <div class="table-responsive">
                @if($members->count())
                    <table class="table table-condensed table-striped">
                        <thead>
                            <tr>
                                <th class="text-center"><input id="check-all" type="checkbox">#</th>
                                <th>Name</th> <th>Email</th>
                                <th class="text-right">OPTIONS</th>
                            </tr>
                        </thead>

                        <tbody id="table-body">
                            @foreach($members as $member)
                                <tr>
                                    <td class="text-center"><input name="check-box" data-id="{{$member->id}}" value="{{$member->id}}" type="checkbox"><strong>{{$member->id}}</strong></td>

                                    <td>{{$member->name}}</td> <td>{{$member->email}}</td>
                                    
                                    <td class="text-right">
                                        <a class="btn btn-xs btn-primary" href="{{ route('members.show', $member->id) }}">
                                            <i class="glyphicon glyphicon-eye-open"></i> 
                                        </a>
                                        
                                        <a class="btn btn-xs btn-warning" href="{{ route('members.edit', $member->id) }}">
                                            <i class="glyphicon glyphicon-edit"></i> 
                                        </a>

                                        <form action="{{ route('members.destroy', $member->id) }}" method="POST" style="display: inline;" onsubmit="return confirm('Delete? Are you sure?');">
                                            {{csrf_field()}}
                                            <input type="hidden" name="_method" value="DELETE">

                                            <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {!! $members->render() !!}
                @else
                    <h3 class="text-center alert alert-info">Empty!</h3>
                @endif
                </div>
            </div>
        </div>
    </div>

</div>

@endsection
<link href="https://cdn.bootcss.com/jquery-modal/0.9.1/jquery.modal.min.css" rel="stylesheet">
@push('css')

@endpush

@push('js')
<script src="https://cdn.bootcss.com/jquery-modal/0.9.1/jquery.modal.min.js"></script>
    <script src="{{ asset('js/jquery.checkall.js') }}"></script>
    <script>
      $(function(){
        $('#check-all').checkAll('#table-body input:checkbox');
      });

      function send_mail() {
        var chk_value =[];
        $('input[name="check-box"]:checked').each(function(){
          chk_value.push($(this).val());
        });
        if(chk_value.length === 0) {
          alert('请选择');
          return false;
        }
        member_ids = _.map(chk_value).join(',');
        url = '/email_send?member_ids=' + member_ids;
        window.location.href = url;
      }
    </script>
@endpush