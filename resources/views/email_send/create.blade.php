@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="col-md-10">
            <div class="panel panel-default">

                <div class="panel-heading">
                    <h1>
                        <i class="glyphicon glyphicon-edit"></i> EmailTemplate /
                        发送邮件
                    </h1>
                </div>
                @include('common.error')
                <div class="panel-body">
                    <form action="{{ route('email_send.store') }}" id="form" method="POST" class="form-horizontal" accept-charset="UTF-8">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="member-multiple" class="col-md-2 control-label">收件人：</label>
                            <div class="col-md-10">
                                <select class="col-md-10" id="member-multiple" name="member_ids[]" multiple="multiple">
                                    <option value="">请选择</option>
                                    @foreach($members as $member)
                                        <option value="{{ $member->id }}">{{ $member->text }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="product-multiple" class="col-md-2 control-label">商品：</label>
                            <div class="col-md-10">
                                <select class="col-md-10" id="product-multiple" name="product_ids[]" multiple="multiple">
                                    <option value="">请选择</option>
                                    @foreach($products as $product)
                                        <option value="{{ $product->id }}" >{{ $product->text }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="template_id" class="col-md-2 control-label">发件模板：</label>
                            <div class="col-md-10">
                                <select class="col-md-10 select-single" name="template_id">
                                    @foreach($templates as $template)
                                        <option value="{{ $template->id }}">{{ $template->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="datetimepicker1" class="col-md-2 control-label">定时发送：</label>
                            <div class="col-md-4">
                                <input type='text' class="form-control" name="plant_at" id='datetimepicker1' value="{{ $suggest_plant_at }}" />
                            </div>
                        </div>
                        <div class="well well-sm">
                            <button type="button" onclick="send()" class="btn btn-primary" id="send-mail">确认发送</button>
                            <button type="button" class="btn btn-success" onclick="check_style()">查看样式</button>
                            <a class="btn btn-link pull-right" href="{{ route('sending_emails.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
                        </div>
                    </form>
                    <div id="template-display"></div>
                 </div>
            </div>
        </div>
    </div>
@endsection
@section('css')
    <link href="https://cdn.bootcss.com/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
@endsection
@push('js')
    <script src="https://cdn.bootcss.com/moment.js/2.20.1/moment.min.js"></script>
    <script src="https://cdn.bootcss.com/moment.js/2.20.1/locale/zh-cn.js"></script>
    <script src="https://cdn.bootcss.com/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
    <script>
      var member_search_uri = '/email_send/search_member';
      var product_search_uri = '/email_send/search_product';
      $(function(){
        $("#datetimepicker1").datetimepicker();
        $('.select-single').select2();
        initProduct(initProductSelector());
        initMember(initMemberSelector());
      });

      function initMemberSelector() {
        return $('#member-multiple').select2({
          ajax: getAjaxJson(member_search_uri),
          placeholder: 'Search for a member',
          escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
          minimumInputLength: 0,
          templateResult: formatMember,
          templateSelection: formatMemberSelection
        });
      }
      // 格式化返回数据
      function formatMember (member) {
        var markup = "<div class='select2-result-repository clearfix'>" +
          "<div class='select2-result-repository__title'>" + member.email + "</div>";

        if (member.name) {
          markup += "<div class='select2-result-repository__description'>" + member.name + "</div>";
        }

        markup += "</div>";

        return markup;
      }

      function formatMemberSelection (member) {
        return member.text || member.name;
      }

      /**
       * 初始化已经选中的用户
       * @param obj
       * @returns {boolean}
       */
      function initMember(member_selector) {
        var member_ids = getParameterByName('member_ids');
        if(!member_ids) {
          return true;
        }
        var url = member_search_uri + '?member_ids=' + member_ids;
        axios.get(url).then(function (response) {
          for (var d = 0; d < response.data.length; d++) {
            var item = response.data[d];
            var option = new Option(item.name, item.id, true, true);
            member_selector.append(option);
          }
          member_selector.trigger('change');
        }).catch(function (error) {
          console.log(error)
        });
      }

      function initProductSelector() {
        return $('#product-multiple').select2({
          ajax: getAjaxJson(product_search_uri),
          placeholder: 'Search for a product',
          escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
          minimumInputLength: 0,
          templateResult: formatProduct,
          templateSelection: formatProductSelection
        });
      }
      // 格式化返回数据
      function formatProduct (product) {
        var markup = "<div class='select2-result-repository clearfix'>" +
          "<div class='select2-result-repository__title'>" + product.name + '-' + product.sku + "</div>";

        markup += "<div class='select2-result-repository__description'>$" + product.price + '| $' + product.promotion_price + "</div>";

        markup += "</div>";

        return markup;
      }

      function formatProductSelection (product) {
        return product.text || product.name;
      }

      function initProduct(selector) {
        var product_ids = getParameterByName('product_ids');
        if(!product_ids) {
          return true;
        }
        var url = product_search_uri + '?product_ids=' + product_ids;
        axios.get(url).then(function (response) {
          console.log(response);
          for (var d = 0; d < response.data.length; d++) {
            var item = response.data[d];
            var option = new Option(item.text, item.id, true, true);
            selector.append(option);
          }
          selector.trigger('change');
        }).catch(function (error) {
          console.log(error)
        });
      }

      function getAjaxJson(url) {
        return {
          url: url,
          dataType: 'json',
          delay: 150,
          data: function (params) {
            return {
              q: params.term, // search term
              page: params.page
            };
          },
          processResults: function (data, params) {
            params.page = params.page || 1;
            return {
              results: data.data,
              pagination: {
                more: (params.page * 15) < data.total
              }
            };
          },
          cache: true
        }
      }
      /* 获取url参数 */
      function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
          results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
      }

      // 查看发送结果样式
      function check_style() {
        var form = $('#form');
        form.attr('action', '/email_send/check_style');
        form.attr('target', '_blank');

        form.submit();

        form.attr('action', '/email_send');
        form.attr('target', '');
      }

      function send() {
        confirm('Are You Sure ?');
        $('#send-mail').addClass('disabled');
        $.ajax({
          type: "POST",
          dataType: "json",
          url: "/email_send" ,
          data: $('#form').serialize(),
          success: function (result) {
            $('#send-mail').removeClass('disabled');
            alert(result.msg);
          },
          error : function() {
            $('#send-mail').removeClass('disabled');
            alert("异常！");
          }
        })
      }
    </script>
@endpush
