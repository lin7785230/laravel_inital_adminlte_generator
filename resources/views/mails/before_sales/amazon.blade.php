<table class="MsoNormalTable" border="0" cellspacing="0" cellpadding="0" width="100%"
       style="width:100.0%;background:#E3E3E3">
    <tbody>
    <tr>
        <td style="padding:5.0pt 5.0pt 5.0pt 5.0pt">
            <div align="center">
                <table class="MsoNormalTable" border="0" cellspacing="0" cellpadding="0" width="640"
                       style="width:480.0pt">
                    <tbody>
                    <tr>
                        <td valign="bottom" style="padding:0cm 0cm 7.5pt 0cm">
                            <table class="MsoNormalTable" border="0" cellspacing="0" cellpadding="0">
                                <tbody>
                                <tr>
                                    <td style="padding:0cm 0cm 0cm 0cm"><p class="MsoNormal">
                                            <span lang="EN-US" style="font-size:7.0pt;font-family:'Verdana',sans-serif;color:#555555">We have <a
                                                        href="{{ config('amazon_mail.home_page') }}"
                                                        target="_blank">recommendations</a> for you. </span></p></td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                        <td style="padding:0cm 0cm 0cm 0cm"></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <p class="MsoNormal"><span lang="EN-US" style="display:none"><o:p>&nbsp;</o:p></span></p>
            <div align="center">
                <table class="MsoNormalTable" border="0" cellspacing="0" cellpadding="0" width="640"
                       style="width:480.0pt;background:#E3E3E3">
                    <tbody>
                    <tr>
                        <td width="100%" style="width:100.0%;padding:0cm 0cm 0cm 0cm">
                            <div style="mso-element:para-border-div;border:none;border-bottom:solid #FF9900 4.5pt;padding:0cm 0cm 0cm 0cm">
                                <p align="center"
                                   style="margin:0cm;margin-bottom:.0001pt;text-align:center;border:none;padding:0cm;max-height: 0px !important">
                                    <span lang="EN-US" style="font-size:1.0pt">&nbsp;<o:p></o:p></span></p>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <p class="MsoNormal"><span lang="EN-US" style="display:none"><o:p>&nbsp;</o:p></span></p>
            <div align="center">
                <table class="MsoNormalTable" border="0" cellspacing="0" cellpadding="0" width="640"
                       style="width:480.0pt">
                    <tbody>
                    <tr>
                        <td style="padding:0cm 0cm 0cm 0cm"></td>
                    </tr>
                    <tr>
                        <td width="100%" style="width:100.0%;background:white;padding:0cm 0cm 0cm 0cm">
                            <div align="center">
                                <table class="MsoNormalTable" border="0" cellspacing="0" cellpadding="0" width="640"
                                       style="width:480.0pt;background:white">
                                    <tbody>
                                    <tr>
                                        <td style="padding:0cm 0cm 0cm 0cm">
                                            <table class="MsoNormalTable" border="0" cellspacing="0" cellpadding="0"
                                                   align="left" style="background:white">
                                                <tbody>
                                                <tr>
                                                    <td style="padding:0cm 0cm 0cm 0cm">
                                                        <p class="MsoNormal"><span lang="EN-US"><a
                                                                        href="{{ config('amazon_mail.home_page') }}"
                                                                        target="_blank"><span
                                                                            style="color:#336699;text-decoration:none"><img
                                                                                border="0" width="110" height="48"
                                                                                style="width:1.1458in;height:.5in"
                                                                                id="_x0000_i1040"
                                                                                src="{{ config('amazon_mail.header_logo') }}"></span></a></span><span
                                                                    lang="EN-US"
                                                                    style="font-size:12.0pt;font-family:SimSun"><o:p></o:p></span>
                                                        </p>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <table class="MsoNormalTable" border="0" cellspacing="0" cellpadding="0"
                                                   align="right">
                                                <tbody>
                                                <tr>
                                                    <td style="padding:5.0pt 5.0pt 5.0pt 5.0pt">
                                                        <div align="right">
                                                            <table class="MsoNormalTable" border="0" cellspacing="0"
                                                                   cellpadding="0">
                                                                <tbody>
                                                                <tr>
                                                                    <td style="padding:0cm 0cm 0cm 0cm"><p
                                                                                class="MsoNormal" align="center"
                                                                                style="text-align:center"><span
                                                                                    lang="EN-US"><a
                                                                                        href="{{config('amazon_mail.home_page')}}"
                                                                                        target="_blank"><b><span style="font-size:9.0pt;font-family:'Arial',sans-serif">
                                                                                            Our Home Page </span></b></a></span>
                                                                        </p></td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="padding:0cm 0cm 0cm 0cm"></td>
                                                                    <td style="padding:0cm 0cm 0cm 0cm"></td>
                                                                    <td style="padding:0cm 0cm 0cm 0cm"></td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <p class="MsoNormal"><span lang="EN-US" style="display:none"><o:p>&nbsp;</o:p></span></p>
            <div align="center">
                <table class="MsoNormalTable" border="0" cellspacing="0" cellpadding="0" width="640"
                       style="width:480.0pt;background:white">
                    <tbody>
                    <tr>
                        <td valign="top" style="padding:4.5pt 11.25pt 9.0pt 11.25pt">
                            <p class="MsoNormal" style="line-height:12.0pt">
                                <span lang="EN-US" style="font-family:'Arial',sans-serif;color:#303942">
                                    {{-- 用户姓名 --}}
                                    {{ $memberName ?? 'Dear Customer'}},
                                </span>
                                <span lang="EN-US" style="font-size:9.0pt;font-family:'Arial',sans-serif;color:#333333"><br>
                                    We’ve found some new deals that you might be interested in.</span>
                                <span lang="EN-US" style="font-size:9.0pt;font-family:'Arial',sans-serif;color:#333333"><o:p></o:p></span>
                            </p>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            @foreach($products as $product)
            <p class="MsoNormal"><span lang="EN-US" style="display:none"><o:p>&nbsp;</o:p></span></p>
            <div align="center">
                <table class="MsoNormalTable" border="0" cellspacing="0" cellpadding="0" width="640"
                       style="width:480.0pt;vertical-align:central">
                    <tbody>
                    <tr>
                        <td width="32%" style="width:32.0%;background:white;padding:4.5pt 0cm 4.5pt 0cm">
                            <table class="MsoNormalTable" border="0" cellspacing="0" cellpadding="0" width="100%"
                                   style="width:100.0%;background:white">
                                <tbody>
                                <tr>
                                    <td style="padding:3.0pt 6.0pt 3.0pt 6.0pt">
                                        <p class="MsoNormal" align="center" style="text-align:center">
                                            <span lang="EN-US">
                                                <a href="{{ $product['link_url'] }}"
                                                        target="_blank">
                                                    <span style="color:#336699;text-decoration:none">
                                                        <img border="0" width="175" height="175" style="width:1.8263in;height:1.8263in" id="_x0000_i1037"
                                                             src="{{ $product['picture_url'] }}">
                                                    </span>
                                                </a>
                                            </span>
                                        </p></td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                        <td width="67%" style="width:67.0%;background:white;padding:4.5pt 0cm 4.5pt 0cm">
                            <div align="right">
                                <table class="MsoNormalTable" border="0" cellspacing="0" cellpadding="0" width="100%"
                                       style="width:100.0%;background:white">
                                    <tbody>
                                    <tr>
                                        <td style="padding:3.0pt 11.25pt 3.0pt 0cm">
                                            <table class="MsoNormalTable" border="0" cellspacing="0" cellpadding="0"
                                                   width="100%" style="width:100.0%">
                                                <tbody>
                                                <tr>
                                                    <td style="padding:0cm 0cm 0cm 0cm">
                                                        <p class="MsoNormal">
                                                            <span lang="EN-US" style="font-size:11.5pt;font-family:'Arial',sans-serif;color:#336699">
                                                                <a href="{{ $product['link_url'] }}"
                                                                        target="_blank">
                                                                    <span style="font-size:10.5pt">{{ $product['name'] }}</span></a> </span>
                                                            <span lang="EN-US"  style="font-size:12.0pt;font-family:SimSun"><o:p></o:p></span>
                                                        </p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding:7.5pt 0cm 0cm 0cm"></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding:0cm 0cm 0cm 0cm">
                                                        <div>
                                                            <p class="MsoNormal" style="line-height:7.5pt"><span
                                                                        lang="EN-US"
                                                                        style="font-size:7.5pt">&nbsp;</span><span
                                                                        lang="EN-US"
                                                                        style="font-size:7.5pt;font-family:SimSun"><o:p></o:p></span>
                                                            </p>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding:0cm 0cm 0cm 0cm">
                                                        <p class="MsoNormal">
                                                            <span class="prpricelabel">
                                                                <span lang="EN-US" style="font-size:9.0pt;font-family:'Arial',sans-serif">
                                                                    List Price:
                                                                </span>
                                                            </span>
                                                            <span lang="EN-US" style="font-size:9.0pt;font-family:'Arial',sans-serif">
                                                                <span style="color:#CC0000">
                                                                    ${{ $product['price'] }}
                                                                </span>
                                                            </span>
                                                            <span lang="EN-US" style="font-size:12.0pt"><o:p></o:p></span>
                                                        </p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding:0cm 0cm 0cm 0cm">
                                                        <p class="MsoNormal">
                                                        <span class="prpricelabel">
                                                            <span lang="EN-US" style="font-size:9.0pt;font-family:'Arial',sans-serif">
                                                                Price:
                                                            </span>
                                                        </span>
                                                            <span ang="EN-US" style="font-size:9.0pt;font-family:'Arial',sans-serif"> </span>
                                                            <span class="prpricehighlight"><b>
                                                                <span lang="EN-US" style="font-size:10.0pt;font-family:'Arial',sans-serif;color:#CC0000">
                                                                    ${{ $product['promotion_price'] }}
                                                                </span></b>
                                                            </span>
                                                            <span lang="EN-US" style="font-size:9.0pt;font-family:'Arial',sans-serif"> </span>
                                                        </p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding:0cm 0cm 0cm 0cm">
                                                        <p class="MsoNormal">
                                                            <span class="prpricelabel">
                                                                <span lang="EN-US" style="font-size:9.0pt;font-family:'Arial',sans-serif">
                                                                    You Save:
                                                                </span>
                                                            </span>
                                                            <span lang="EN-US" style="font-size:9.0pt;font-family:'Arial',sans-serif">
                                                                <span style="color:#CC0000">
                                                                    ${{($product['price']- $product['promotion_price'])}} ({{ round((($product['price']- $product['promotion_price'])/$product['price']) * 100) }}%)
                                                                </span>
                                                            </span>
                                                        </p>
                                                    </td>
                                                </tr>
                                                <tr style="height:5.0pt">
                                                    <td style="padding:0cm 0cm 0cm 0cm;height:5.0pt">
                                                        <div>
                                                            <p class="MsoNormal" style="line-height:6.0pt"><span
                                                                        lang="EN-US" style="font-size:6.0pt">&nbsp;<o:p></o:p></span>
                                                            </p>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding:0cm 0cm 0cm 0cm">
                                                        <p class="MsoNormal">
                                                            <span lang="EN-US" style="font-size:9.0pt;font-family:'Arial',sans-serif;color:#555555">
                                                                {{ $product['desc'] }} ...
                                                                <a href="{{ $product['link_url'] }}" target="_blank">Read More</a>
                                                            </span>
                                                            <span lang="EN-US" style="font-size:12.0pt"><o:p></o:p></span>
                                                        </p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding:0cm 0cm 0cm 0cm">
                                                        <div>
                                                            <p class="MsoNormal" style="line-height:6.0pt">
                                                                <span lang="EN-US" style="font-size:6.0pt">&nbsp;<o:p></o:p></span>
                                                            </p>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding:0cm 0cm 0cm 0cm;border-radius:5px">
                                                        <table class="MsoNormalTable" border="0" cellspacing="0"
                                                               cellpadding="0" width="80%" style="width:80.0%">
                                                            <tbody>
                                                            <tr style="height:17.0pt">
                                                                <td style="background:#FFC435;padding:0cm 0cm 0cm 0cm;height:17.0pt">
                                                                    <p class="MsoNormal" align="center"
                                                                       style="text-align:center">
                                                                        <b>
                                                                        <span lang="EN-US" style="font-family:'Arial',sans-serif">
                                                                            <a href="{{ $product['link_url'] }}"
                                                                                target="_blank">
                                                                                <span style="color:#303942">
                                                                                Learn more
                                                                                </span>
                                                                            </a> <o:p></o:p>
                                                                        </span>
                                                                        </b>
                                                                    </p>
                                                                </td>
                                                                <td style="padding:0cm 0cm 0cm 0cm;height:17.0pt">
                                                                    <p class="MsoNormal" align="left"
                                                                       style="text-align:left"><span
                                                                                lang="EN-US">&nbsp;</span><span
                                                                                lang="EN-US"
                                                                                style="font-size:12.0pt;font-family:SimSun"><o:p></o:p></span>
                                                                    </p>
                                                                </td>
                                                                <td width="30%"
                                                                    style="width:30.0%;background:#E7E7C2;padding:0cm 0cm 0cm 0cm;height:17.0pt;border-radius:5px">
                                                                    <p class="MsoNormal" align="center"
                                                                       style="text-align:center"><b>
                                                                            <span lang="EN-US" style="font-size:9.0pt;font-family:'Arial',sans-serif">
                                                                                <a href="{{ $product['link_url'] }}"
                                                                                        target="_blank"><span
                                                                                            style="color:#2F2F79">Add to Wish List</span></a> <o:p></o:p></span></b>
                                                                    </p>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            @endforeach
            <p class="MsoNormal"><span lang="EN-US" style="display:none"><o:p>&nbsp;</o:p></span></p>
            <div align="center">
                <table class="MsoNormalTable" border="0" cellspacing="0" cellpadding="0" width="640"
                       style="width:480.0pt;background:white">
                    <tbody>
                    <tr>
                        <td style="padding:0cm 0cm 0cm 0cm">
                            <p class="MsoNormal"><span lang="EN-US" style="display:none"><o:p>&nbsp;</o:p></span></p>
                            <div align="center">
                                <table class="MsoNormalTable" border="0" cellspacing="0" cellpadding="0" width="640"
                                       style="width:480.0pt;background:white">
                                    <tbody>
                                    <tr>
                                        <td style="padding:0cm 0cm 0cm 0cm">
                                            <p class="MsoNormal" align="center" style="text-align:center"><span
                                                        lang="EN-US"><img border="0" width="640" height="8"
                                                                          style="width:6.6666in;height:.0833in"
                                                                          id="_x0000_i1029"
                                                                          src="http://g-ec2.images-amazon.com/images/G/01/template/section-div_640x8.jpg"></span><span
                                                        lang="EN-US" style="font-size:12.0pt;font-family:SimSun"><o:p></o:p></span>
                                            </p>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <p class="MsoNormal"><span lang="EN-US" style="display:none"><o:p>&nbsp;</o:p></span></p>
                            <div align="center">
                                <table class="MsoNormalTable" border="0" cellspacing="0" cellpadding="0" width="640"
                                       style="width:480.0pt;background:white">
                                    <tbody>
                                    <tr>
                                        <td width="50%" style="width:50.0%;padding:4.0pt 4.0pt 4.0pt 4.0pt">
                                            <table class="MsoNormalTable" border="0" cellspacing="0" cellpadding="0"
                                                   align="left">
                                                <tbody>
                                                <tr>
                                                    <td style="padding:3.0pt 3.0pt 3.0pt 3.0pt"><p class="MsoNormal"
                                                                                                   align="left"
                                                                                                   style="text-align:left">
                                                            <span lang="EN-US"><a
                                                                        href="https://www.amazon.com/gp/r.html?C=1A09FXKRGWZJE&amp;K=2Q4IDAC5O2URQ&amp;M=urn:rtfm:msg:20180110081333a3f3fafb6ca94c73aeef3c23b691c023&amp;R=3PVCBCM7S0UU2&amp;T=C&amp;U=http%3A%2F%2Fwww.amazon.com%2Fref%3Dpe_2565310_267464980_pe_footer_amazonlogo%2F&amp;H=MD0PM8I4ZTUQV4EXOKKXUYWZDL0A&amp;ref_=pe_2565310_267464980_pe_footer_amazonlogo"
                                                                        target="_blank"></a></span><!--[if !vml]--><a
                                                                    href="https://www.amazon.com/gp/r.html?C=1A09FXKRGWZJE&amp;K=2Q4IDAC5O2URQ&amp;M=urn:rtfm:msg:20180110081333a3f3fafb6ca94c73aeef3c23b691c023&amp;R=3PVCBCM7S0UU2&amp;T=C&amp;U=http%3A%2F%2Fwww.amazon.com%2Fref%3Dpe_2565310_267464980_pe_footer_amazonlogo%2F&amp;H=MD0PM8I4ZTUQV4EXOKKXUYWZDL0A&amp;ref_=pe_2565310_267464980_pe_footer_amazonlogo"
                                                                    target="_blank"><img border="0" width="126"
                                                                                         height="24"
                                                                                         style="width:1.3125in;height:.25in"
                                                                                         src="{{ config('amazon_mail.footer_logo') }}"
                                                                                         align="left"
                                                                                         v:shapes="image_x0020_2"></a>
                                                            <!--[endif]--><span lang="EN-US"><a
                                                                        href="https://www.amazon.com/gp/r.html?C=1A09FXKRGWZJE&amp;K=2Q4IDAC5O2URQ&amp;M=urn:rtfm:msg:20180110081333a3f3fafb6ca94c73aeef3c23b691c023&amp;R=3PVCBCM7S0UU2&amp;T=C&amp;U=http%3A%2F%2Fwww.amazon.com%2Fref%3Dpe_2565310_267464980_pe_footer_amazonlogo%2F&amp;H=MD0PM8I4ZTUQV4EXOKKXUYWZDL0A&amp;ref_=pe_2565310_267464980_pe_footer_amazonlogo"
                                                                        target="_blank"></a></span></p></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                        <td style="padding:4.0pt 4.0pt 4.0pt 4.0pt">
                                            <table class="MsoNormalTable" border="0" cellspacing="0" cellpadding="0"
                                                   align="right">
                                                <tbody>
                                                <tr>
                                                    <td style="padding:3.0pt 3.0pt 3.0pt 3.0pt">
                                                        <p class="MsoNormal" align="right" style="text-align:right">
                                                            <span lang="EN-US"
                                                                  style="font-size:7.5pt;font-family:'Arial',sans-serif;color:#666666">Connect with us</span><span
                                                                    lang="EN-US"
                                                                    style="font-size:12.0pt;font-family:SimSun"><o:p></o:p></span>
                                                        </p>
                                                    </td>
                                                    <td style="padding:3.0pt 3.0pt 3.0pt 3.0pt"><p class="MsoNormal"
                                                                                                   align="left"
                                                                                                   style="text-align:left">
                                                            <span lang="EN-US"><a
                                                                        href="{{ config('amazon_mail.facebook', 'https://www.facebook.com') }}"
                                                                        target="_blank"><span
                                                                            style="font-size:7.5pt;font-family:'Verdana',sans-serif;color:#336699;text-decoration:none"><img
                                                                                border="0" width="30" height="30"
                                                                                style="width:.3125in;height:.3125in"
                                                                                id="_x0000_i1028"
                                                                                src="http://g-ecx.images-amazon.com/images/G/01/us-mail/FBlogo.gif"></span></a></span>
                                                        </p>
                                                    </td>
                                                    @if(config('amazon_mail.twitter'))
                                                    <td style="padding:3.0pt 3.0pt 3.0pt 3.0pt"><p class="MsoNormal">
                                                            <span lang="EN-US"><a
                                                                        href="{{ config('amazon_mail.twitter') }}"
                                                                        target="_blank"><span
                                                                            style="font-size:7.5pt;font-family:'Verdana',sans-serif;color:#336699;text-decoration:none"><img
                                                                                border="0" width="30" height="30"
                                                                                style="width:.3125in;height:.3125in"
                                                                                id="_x0000_i1027"
                                                                                src="http://g-ecx.images-amazon.com/images/G/01/us-mail/TwitterLogo.gif"></span></a></span>
                                                        </p></td>
                                                    @endif
                                                    @if(config('amazon_mail.pinterest'))
                                                    <td style="padding:3.0pt 3.0pt 3.0pt 3.0pt"><p class="MsoNormal">
                                                            <span lang="EN-US"><a
                                                                        href="{{ config('amazon_mail.pinterest') }}"
                                                                        target="_blank"><span
                                                                            style="font-size:7.5pt;font-family:'Verdana',sans-serif;color:#336699;text-decoration:none"><img
                                                                                border="0" width="30" height="30"
                                                                                style="width:.3125in;height:.3125in"
                                                                                id="_x0000_i1026"
                                                                                src="http://g-ecx.images-amazon.com/images/G/01/us-mail/pinterest_favicon_30x30.png"></span></a></span>
                                                        </p></td>
                                                    @endif

                                                </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <p class="MsoNormal"><span lang="EN-US" style="display:none"><o:p>&nbsp;</o:p></span></p>
                            <div align="center">
                                <table class="MsoNormalTable" border="0" cellspacing="0" cellpadding="0" width="640"
                                       style="width:480.0pt;background:white">
                                    <tbody>
                                    <tr>
                                        <td width="100%" style="width:100.0%;padding:0cm 0cm 0cm 0cm">
                                            <div style="mso-element:para-border-div;border:none;border-bottom:solid #666666 2.25pt;padding:0cm 0cm 0cm 0cm">
                                                <p align="center"
                                                   style="margin:0cm;margin-bottom:.0001pt;text-align:center;border:none;padding:0cm;max-height: 0px !important">
                                                    <span lang="EN-US" style="font-size:1.0pt">&nbsp;<o:p></o:p></span>
                                                </p>
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <p class="MsoNormal"><span lang="EN-US" style="display:none"><o:p>&nbsp;</o:p></span></p>
                            <table class="MsoNormalTable" border="0" cellspacing="0" cellpadding="0" width="640"
                                   style="width:480.0pt;background:#E3E3E3">
                                <tbody>
                                <tr style="height:5.0pt">
                                    <td width="100%" style="width:100.0%;padding:1.0pt 1.0pt 1.0pt 1.0pt;height:5.0pt">
                                        <div>
                                            <p class="MsoNormal" style="line-height:7.5pt"><span lang="EN-US"
                                                                                                 style="font-size:7.5pt">&nbsp;<o:p></o:p></span>
                                            </p>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding:1.0pt 1.0pt 1.0pt 1.0pt">
                                        <p class="MsoNormal">
                                            <span lang="EN-US" style="font-size:7.5pt;font-family:'Arial',sans-serif;color:#666666">
                                                {{ config('amazon_mail.notes') }}
                                            </span>
                                        </p>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </td>
    </tr>
    </tbody>
</table>