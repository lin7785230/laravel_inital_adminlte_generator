<img width="1" height="1" src="https://www.amazon.com/gp/r.html?C=1518172TEEOSN&amp;K=2Q4IDAC5O2URQ&amp;M=urn:rtn:msg:20180126080841f0aa6528a9e34beda57e1a3d0640p0na&amp;R=3PVCBCM7S0UU2&amp;T=O&amp;U=https%3A%2F%2Fimages-na.ssl-images-amazon.com%2Fimages%2FG%2F01%2Fnav%2Ftransp.gif&amp;H=NI1OAWAPFKFUAWKRAVMNROP56DKA&amp;ref_=pe_3461610_269935100_opens">
<table width="100%" border="0" cellpadding="10" cellspacing="0" style="background-color: #e3e3e3;mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-collapse: collapse;border-collapse: collapse" yahoo="fix">
    <tbody>
    <tr>
        <td valign="middle">
            <table width="640" border="0" align="center" cellpadding="0" cellspacing="0" class="deviceWidth" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-collapse: collapse;border-collapse: collapse">
                <tbody>
                <tr>
                    <td style="padding:0 0 10px 0; font-size:0; height:0;" class="x-site" valign="bottom">
                        <table cellspacing="0" cellpadding="0" border="0" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-collapse: collapse;border-collapse: collapse">
                            <tbody>
                            <tr>
                                <td> <span class="email_super_subject" style="font-family:verdana,arial,sans-serif;font-size:9px;line-height:11px;color:#555;">
                                        We’ve found deals for you.
                                    </span>
                                </td>
                            </tr>
                            </tbody>
                        </table> </td>
                    <td align="right" class="header-csslot">
                        <table cellspacing="0" cellpadding="0" border="0" class="cs-image-slot" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-collapse: collapse;border-collapse: collapse">
                            <tbody>
                            <tr>
                                <td> <map class="image-csslot-normal" name="email-header-right-1-0"> <area href="https://www.amazon.com/gp/r.html?C=1518172TEEOSN&amp;K=2Q4IDAC5O2URQ&amp;M=urn:rtn:msg:20180126080841f0aa6528a9e34beda57e1a3d0640p0na&amp;R=3PVCBCM7S0UU2&amp;T=C&amp;U=https%3A%2F%2Fwww.amazon.com%2Fb%2F%3F%26node%3D1240668011%26amp%3Bref%3DNYNY_xsite_010118%26ref_%3Dpe_3461610_269935100&amp;H=ERRX56BGDIMZ70GYWDSWJPJA5NKA&amp;ref_=pe_3461610_269935100" shape="rect" coords="0,0,300,50" target="_blank"> </map>
                                    <div class="image-csslot-normal">
                                        {{--<img border="0" align="middle" usemap="#email-header-right-1-0" src="https://m.media-amazon.com/images/G/01/gateway/092330_us_health__personal_care_new_year_you_300x50_v4._CB1513893453_.jpg" style="height: auto; outline: none; border: 0 none; -ms-interpolation-mode: bicubic;width: 100% !important; height: auto !important">--}}
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table> </td>
                </tr>
                </tbody>
            </table>
            <table width="640" height="6" border="0" align="center" cellpadding="0" cellspacing="0" class="deviceWidth" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-collapse: collapse;border-collapse: collapse">
                <tbody>
                <tr>
                    <td width="100%" bgcolor="#e3e3e3" align="center" style="border-bottom: 6px solid #FF9900; height: 0px !important; max-height: 0px !important; font-size:0px; margin: 0;"> <p>&nbsp;</p> </td>
                </tr>
                </tbody>
            </table>
            <table width="640" cellpadding="0" cellspacing="0" bgcolor="#ffffff" border="0" align="center" style="background-color:#ffffff;mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-collapse: collapse;border-collapse: collapse" class="deviceWidth">
                <tbody>
                <tr>
                    <td>
                        <table width="100%" height="5px" border="0" cellpadding="0" cellspacing="0" align="center" class="deviceWidth" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-collapse: collapse;border-collapse: collapse">
                            <tbody>
                            <tr>
                                <td></td>
                            </tr>
                            <tr>
                                <td width="100%" valign="middle">

                                    <table bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" class="logoTop" align="left" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-collapse: collapse;border-collapse: collapse">
                                        <tbody>
                                        <tr>
                                            <td class="center">
                                                <a href="{{ config('amazon_mail.home_page') }}" target="_blank">
                                                    <img src="{{ config('amazon_mail.header_logo') }}" border="0" style="padding:6px;height: auto; outline: none; border: 0 none; -ms-interpolation-mode: bicubic" class="amazonLogo">
                                                </a>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table class="logoTop " border="0" cellpadding="10" cellspacing="0" align="right" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-collapse: collapse;border-collapse: collapse">
                                        <tbody>
                                        <tr>
                                            <td align="right" class="topMenu">
                                                <table cellpadding="0" cellspacing="0" style="margin:0 auto;mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-collapse: collapse;border-collapse: collapse">
                                                    <tbody>
                                                    <tr>
                                                        <td align="center">
                                                            <a href="{{ config('amazon_mail.home_page') }}" style="font-family: arial,verdana,sans-serif; font-size: 12px; color: rgb(51, 102, 153); text-decoration: underline; font-weight: bold;text-align:center;word-wrap: normal ! important;" target="_blank">
                                                                See All
                                                            </a>
                                                        </td>
                                                        <td rowspan="2" width="4" class="x-site">&nbsp;</td>
                                                    </tr>
                                                    </tbody>
                                                </table> </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>

                        <table width="640" class="deviceWidth" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#FFFFFF" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-collapse: collapse;border-collapse: collapse">
                            <tbody>
                            <tr>
                                <td align="center"> <img src="https://images-na.ssl-images-amazon.com/images/G/01/template/section-div_640x8.jpg" border="0" align="middle" class="deviceWidth" style="   ; ; ; ; ; ; ;  "> </td>
                            </tr>
                            </tbody>
                        </table>
                        <table width="610" cellspacing="0" cellpadding="15" border="0" align="center" class="deviceWidth" style="width:100%;mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-collapse: collapse;border-collapse: collapse">
                            <tbody>
                            <tr>
                                <td class="intro-text" align="left" style="text-align:left;font-size: 12px; color: rgb(51, 51, 51); font-weight: normal; text-align: left; font-family: Arial, sans-serif; line-height: 16px; vertical-align: top; padding: 6px 15px 12px 15px"> <span class="salutation-text" style="text-decoration: none; color: rgb(48, 57, 66); font-size: 14px; color: rgb(48, 57, 66); font-weight: normal">
                                        {{ $memberName ?? 'Dear Customer'}},
                                    </span>
                                    <br>
                                    We’ve found some new deals that you might be interested in.
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        @foreach($products as $product)
                        <table width="640" class="deviceWidth" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#FFFFFF" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-collapse: collapse;border-collapse: collapse">
                            <tbody>
                            <tr>
                                <td align="center"> <img src="https://images-na.ssl-images-amazon.com/images/G/01/template/section-div_640x8.jpg" border="0" align="middle" class="deviceWidth" style="   ; ; ; ; ; ; ;  "> </td>
                            </tr>
                            </tbody>
                        </table>
                        <table width="640" cellspacing="0" cellpadding="0" border="0" align="center" class="deviceWidth" style="vertical-align:central;mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-collapse: collapse;border-collapse: collapse">
                            <tbody>
                            <tr>
                                <td width="32%" bgcolor="#ffffff" align="left" class="pfe_col" style="padding:8px;" valign="top">
                                    <table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-collapse: collapse;border-collapse: collapse">
                                        <tbody>
                                        <tr>
                                            <td valign="bottom" align="center" style="vertical-align: middle; padding:8px 8px 8px 8px;">
                                                <a href="{{ $product['link_url'] }}" target="_blank">
                                                    <img class="oma_deal_image" src="{{ $product['picture_url'] }}" width="300" border="0" style="height: auto; outline: none; border: 0 none; -ms-interpolation-mode: bicubic">
                                                </a>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table> </td>
                                <td width="67%" valign="top" bgcolor="#ffffff" align="right" style="padding:6px 0" class="pfe_col">
                                    <table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-collapse: collapse;border-collapse: collapse">
                                        <tbody>
                                        <tr>
                                            <td valign="middle" align="left" class="pe_asin" style="font-size: 14px; color: #333333; font-weight: normal; text-align: left; font-family:Arial, sans-serif; line-height: 14px; vertical-align: middle; padding:4px 15px 4px 0">
                                                <table width="100%" cellspacing="0" cellpadding="0" border="0" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-collapse: collapse;border-collapse: collapse">
                                                    <tbody>
                                                    <tr>
                                                        <td valign="top" align="left"> <span style="font-family:Arial,Helvetica,Sans-Serif;font-size:18px;font-weight:bold;font-style:normal;text-decoration:none;color:#369;text-align:left;">
                                                        <a class="oma_deal_title" style="font-family:Arial,Helvetica,Sans-Serif;font-size:18px;font-weight:bold;font-style:normal;color:#369;text-align:left;line-height:25px;"
                                                           href="{{ $product['link_url'] }}"
                                                           target="_blank">
                                                            {{ $product['name'] }}
                                                        </a>
                                                    </span>
                                                        </td>
                                                    </tr>
                                                    <tr height="8">
                                                        <td>
                                                            <div width="640" class="spacer" style="font-size:8px;line-height:8px;">
                                                                &nbsp;
                                                            </div> </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top">
                                                            <font size="2" face="arial,Helvetica,sans-serif">
                                                                <font size="2" face="arial,Helvetica,sans-serif">
                                                                    <span class="oma_deal_dprice" style="font-size:18px;line-height:24px;">
                                                                    <span style="white-space:nowrap;"><s>${{ $product['price'] }}</s></span>
                                                                    </span>
                                                                    <br>
                                                                    Price:
                                                            <span class="oma_deal_dprice" style="font-size:18px;line-height:24px;color:#990000;font-weight:bold;">
                                                                <span style="white-space:nowrap;">${{ $product['promotion_price'] }}</span>
                                                            </span>
                                                                </font>
                                                            </font>
                                                        </td>
                                                    </tr>
                                                    <tr height="8">
                                                        <td>
                                                            <div width="640" class="spacer" style="font-size:8px;line-height:8px;">
                                                                &nbsp;
                                                            </div> </td>
                                                    </tr>
                                                    <tr class="pr_description">
                                                        <td align="left">
                                                    <span class="oma_deal_description" style="font-family: Arial,Helvetica,sans-serif; text-decoration:none; font-weight:normal; color:#555555; font-size:13px;text-align:left;line-height: 18px;">
                                                        {{ $product['desc'] }}
                                                    </span>
                                                        </td>
                                                    </tr>
                                                    <tr class="spacer">
                                                        <td height="10">
                                                            <div width="1" class="spacer" style="font-size:8px;line-height:8px;">
                                                                &nbsp;
                                                            </div> </td>
                                                    </tr>
                                                    <tr class="pr_learn_more">
                                                        <td>
                                                            <table cellspacing="0" cellpadding="0" border="0" height="40" width="100%" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-collapse: collapse;border-collapse: collapse">
                                                                <tbody>
                                                                <tr>
                                                                    <td valign="middle" align="center" height="40" style="background-color:#FFC435;border-radius:5px;font-family:Arial, sans-serif;font-size:14px;font-weight:bold;">
                                                                        <a class="oma_big_lmbutton" href="{{ $product['link_url'] }}" style="display:block;text-decoration:none;color:#303942;white-space:nowrap;padding:10px;"
                                                                           target="_blank">
                                                                            Learn more
                                                                        </a>
                                                                    </td>
                                                                    <td width="35%" class="x-site-tablet">&nbsp;</td>
                                                                </tr>
                                                                </tbody>
                                                            </table> </td>
                                                    </tr>
                                                    </tbody>
                                                </table> </td>
                                        </tr>
                                        </tbody>
                                    </table> </td>                            </tr>
                            </tbody>
                        </table>
                        @endforeach
                </tr>
                </tbody>
            </table>
            <table width="640" class="deviceWidth" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#FFFFFF" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-collapse: collapse;border-collapse: collapse">
                <tbody>
                <tr>
                    <td align="center"> <img src="https://images-na.ssl-images-amazon.com/images/G/01/template/section-div_640x8.jpg" border="0" align="middle" class="deviceWidth" style="   ; ; ; ; ; ; ;  "> </td>
                </tr>
                </tbody>
            </table>
            <table class="cs-slot deviceWidth" cellspacing="0" cellpadding="0" border="0" align="center" width="640" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-collapse: collapse;border-collapse: collapse">
                <tbody>
                <tr>
                    <td>
                        <table width="640" class="deviceWidth" border="0" align="center" cellpadding="8" cellspacing="0" bgcolor="#FFFFFF" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-collapse: collapse;border-collapse: collapse">
                            <tbody>
                            <tr>
                                <td width="50%">
                                    <table border="0" align="left" cellpadding="6" cellspacing="0" class="settings" cborder="0" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-collapse: collapse;border-collapse: collapse">
                                        <tbody>
                                        <tr>
                                            <td align="left">
                                                <a href="{{ config('amazon_mail.home_page') }}" target="_blank">
                                                    <img src="{{ config('amazon_mail.footer_logo') }}" border="0" align="left" style="height: auto; outline: none; border: 0 none; -ms-interpolation-mode: bicubic">
                                                </a>
                                                <a href="https://www.amazon.com/gp/r.html?C=1518172TEEOSN&amp;K=2Q4IDAC5O2URQ&amp;M=urn:rtn:msg:20180126080841f0aa6528a9e34beda57e1a3d0640p0na&amp;R=3PVCBCM7S0UU2&amp;T=C&amp;U=http%3A%2F%2Fwww.amazon.com%2Fref%3Dpe_3461610_269935100_sef_logo&amp;H=FC3EFCCSTR5XGY7PT0TBFZPLNRKA&amp;ref_=pe_3461610_269935100_sef_logo" target="_blank"></a>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table></td>
                                <td align="right">
                                    <table class="connect" border="0" cellpadding="6" cellspacing="0" align="right" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-collapse: collapse;border-collapse: collapse">
                                        <tbody>
                                        <tr>
                                            <td align="right">
                                                <span style="font-family:arial,sans-serif; font-size:10px; line-height:10px; color:#666">
                                                    Connect with us
                                                </span>
                                            </td>
                                            <td class="facebook">
                                                <font style="font-family: verdana,arial,sans-serif; font-size: 10px; color: #999;" color="#999" face="verdana,arial,sans-serif" size="1">
                                                    <a href="{{ config('amazon_mail.facebook', 'https://www.facebook.com') }}" target="_blank">
                                                        <img src="http://g-ecx.images-amazon.com/images/G/01/us-mail/FBlogo.gif" border="0" height="30" hspace="0" vspace="0" width="30" style="width:30px; height:30px;height: auto; outline: none; border: 0 none; -ms-interpolation-mode: bicubic">
                                                    </a>
                                                </font>
                                            </td>
                                            @if(config('amazon_mail.twitter'))
                                            <td class="twitter">
                                                <font style="font-family: verdana,arial,sans-serif; font-size: 10px; color: #999;" color="#999" face="verdana,arial,sans-serif" size="1">
                                                    <a href="{{ config('amazon_mail.twitter') }}" target="_blank">
                                                        <img src="http://g-ecx.images-amazon.com/images/G/01/us-mail/TwitterLogo.gif" border="0" height="30" hspace="0" vspace="0" width="30" style="width:30px; height:30px;height: auto; outline: none; border: 0 none; -ms-interpolation-mode: bicubic">
                                                    </a>
                                                </font>
                                            </td>
                                            @endif
                                            @if(config('amazon_mail.pinterest'))
                                                <td class="x-site"><font style="font-family: verdana,arial,sans-serif; font-size: 10px; color: #999;" color="#999" face="verdana,arial,sans-serif" size="1">
                                                        <a href="{{ config('amazon_mail.pinterest') }}" target="_blank">
                                                            <img src="http://g-ecx.images-amazon.com/images/G/01/us-mail/pinterest_favicon_30x30.png" border="0" height="30" hspace="0" vspace="0" width="30" style="height: auto; outline: none; border: 0 none; -ms-interpolation-mode: bicubic">
                                                        </a>
                                                    </font>
                                                </td>
                                            @endif
                                        </tr>
                                        </tbody>
                                    </table></td>
                            </tr>
                            </tbody>
                        </table>
                        <table width="640" cellspacing="0" cellpadding="0" border="0" bgcolor="white" align="center" height="3" class="deviceWidth" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-collapse: collapse;border-collapse: collapse">
                            <tbody>
                            <tr>
                                <td width="100%" bgcolor="white" align="center"><p width="100%" style="border-bottom: 3px solid #666666; height: 0px !important; max-height: 0px !important; font-size:0px; margin: 0;">&nbsp;</p></td>
                            </tr>
                            </tbody>
                        </table> </td>
                </tr>
                </tbody>
            </table>
            <table align="center" width="640" border="0" cellpadding="2" cellspacing="0" class="sef_legal_footer" style="background-color:#e3e3e3;mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-collapse: collapse;border-collapse: collapse;background-color: rgb(227, 227, 227)" bgcolor="#e3e33">
                <tbody>
                <tr height="10">
                    <td width="100%">
                        <div width="100%" style="font-size:10px; line-height:10px">
                            &nbsp;
                        </div></td>
                </tr>
                <tr>
                    <td align="left" class="legal-text" style="font-family: Arial, Helvetica, Sans-Serif; font-size: 10px; color: rgb(102, 102, 102)">
                        We hope you found this message to be useful.
                        <br> Please note that this message was sent to the following e-mail address: <span class="legal-text-c2" style="font-family: Arial, Helvetica, Sans-Serif; font-size: 10px; color: rgb(191, 191, 191)">
                            <a href="mailto:{{ $memberEmail }}" target="_blank">
                                {{ $memberEmail }}
                            </a>
                        </span>
                    </td>
                </tr>
                </tbody>
            </table> </td>
    </tr>
    </tbody>
</table>
<table cellpadding="0" cellspacing="0" border="0" class="mobile-hide-gmail" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-collapse: collapse;border-collapse: collapse">
    <tbody>
    <tr>
        <td height="1" class="hide" style="min-width:620px; font-size:0px;line-height:0px;"> <img height="1" style="min-width: 620px; text-decoration: none; border: none; -ms-interpolation-mode: bicubic;height: auto; outline: none; border: 0 none; -ms-interpolation-mode: bicubic"> </td>
    </tr>
    </tbody>
</table>
<img width="1" height="1" src="https://www.amazon.com/gp/r.html?C=1518172TEEOSN&amp;K=2Q4IDAC5O2URQ&amp;M=urn:rtn:msg:20180126080841f0aa6528a9e34beda57e1a3d0640p0na&amp;R=3PVCBCM7S0UU2&amp;T=E&amp;U=https%3A%2F%2Fimages-na.ssl-images-amazon.com%2Fimages%2FG%2F01%2Fnav%2Ftransp.gif&amp;H=POHHWBZEN3OE0WP02UAHS5IZEWUA&amp;ref_=pe_3461610_269935100_open">