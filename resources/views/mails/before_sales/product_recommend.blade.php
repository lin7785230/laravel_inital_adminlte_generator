<div>
    Thank you again for your order of {{ $product['name'] }}.
    The Price is : {{ $product['price'] }}, sale price: {{ $product['sale_price'] }}.
    See the details on your product here:  {{ $product['link'] }}
    The available tracking information shows that your item is out for delivery today.

    Thank You,

    Greg
</div>
{!! $content !!}