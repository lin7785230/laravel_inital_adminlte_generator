@extends('layouts.admin')
@section('content')
    {!! $dataTable->table() !!}
@endsection
@push('js')
<script src="/vendor/datatables/buttons.server-side.js"></script>
{!! $dataTable->scripts() !!}

@endpush