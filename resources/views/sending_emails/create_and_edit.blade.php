@extends('layouts.admin')

@section('content')

<div class="container">
    <div class="col-md-10">
        <div class="panel panel-default">
            
            <div class="panel-heading">
                <h1>
                    <i class="glyphicon glyphicon-edit"></i> SendingEmail /
                    @if($sending_email->id)
                        Edit #{{$sending_email->id}}
                    @else
                        Create
                    @endif
                </h1>
            </div>

            @include('common.error')

            <div class="panel-body">
                @if($sending_email->id)
                    <form action="{{ route('sending_emails.update', $sending_email->id) }}" method="POST" accept-charset="UTF-8">
                        <input type="hidden" name="_method" value="PUT">
                @else
                    <form action="{{ route('sending_emails.store') }}" method="POST" accept-charset="UTF-8">
                @endif

                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    
                <div class="form-group">
                    <label for="id-field">Id</label>
                    <input class="form-control" type="text" name="id" id="id-field" value="{{ old('id', $sending_email->id ) }}" />
                </div> 
                <div class="form-group">
                    <label for="email_template_id-field">Email_template_id</label>
                    <input class="form-control" type="text" name="email_template_id" id="email_template_id-field" value="{{ old('email_template_id', $sending_email->email_template_id ) }}" />
                </div> 
                <div class="form-group">
                    <label for="member_id-field">Member_id</label>
                    <input class="form-control" type="text" name="member_id" id="member_id-field" value="{{ old('member_id', $sending_email->member_id ) }}" />
                </div> 
                <div class="form-group">
                    <label for="send_status-field">Send_status</label>
                    <input class="form-control" type="text" name="send_status" id="send_status-field" value="{{ old('send_status', $sending_email->send_status ) }}" />
                </div> 
                <div class="form-group">
                    <label for="send_at-field">Send_at</label>
                    <input class="form-control" type="text" name="send_at" id="send_at-field" value="{{ old('send_at', $sending_email->send_at ) }}" />
                </div>

                    <div class="well well-sm">
                        <button type="submit" class="btn btn-primary">Save</button>
                        <a class="btn btn-link pull-right" href="{{ route('sending_emails.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection