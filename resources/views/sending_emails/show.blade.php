@extends('layouts.admin')

@section('content')

<div class="container">
    <div class="col-md-10">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1>SendingEmail / Show #{{ $sending_email->id }}</h1>
            </div>

            <div class="panel-body">
                <div class="well well-sm">
                    <div class="row">
                        <div class="col-md-6">
                            <a class="btn btn-link" href="{{ route('sending_emails.index') }}"><i class="glyphicon glyphicon-backward"></i> Back</a>
                        </div>
                        <div class="col-md-6">
                             <a class="btn btn-sm btn-warning pull-right" href="{{ route('sending_emails.edit', $sending_email->id) }}">
                                <i class="glyphicon glyphicon-edit"></i> Edit
                            </a>
                        </div>
                    </div>
                </div>

                <label>Id</label>
<p>
	{{ $sending_email->id }}
</p> <label>Email_template_id</label>
<p>
	{{ $sending_email->email_template_id }}
</p> <label>Member_id</label>
<p>
	{{ $sending_email->member_id }}
</p> <label>Send_stauts</label>
<p>
	{{ $sending_email->send_stauts }}
</p> <label>Send_at</label>
<p>
	{{ $sending_email->send_at }}
</p> <label>Nullable</label>
<p>
	{{ $sending_email->nullable }}
</p>
            </div>
        </div>
    </div>
</div>

@endsection
