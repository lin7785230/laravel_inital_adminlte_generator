@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="col-md-10">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1>
                    <i class="glyphicon glyphicon-align-justify"></i> SendingEmail
                    <a class="btn btn-success pull-right" href="{{ route('email_send.create') }}"><i class="glyphicon glyphicon-plus"></i> Create</a>
                    <button type="button" id="retry_all" class="btn btn-danger pull-right" onclick="retry_all()">重新发送所有失败</button>
                </h1>
            </div>

            <div class="panel-body">
                <form class="form-inline" action="{{route('sending_emails.index')}}">
                    <label for="search-send_status">Status</label>
                    <select name="send_status" id="search-send_status" class="form-control">
                        <option value="">全部</option>
                        @foreach($status_map as $key => $value)
                            <option value="{{ $key }}" @if(''!=request('send_status') && request('send_status') == $key) selected @endif>{{ $value }}</option>
                        @endforeach
                    </select>
                    <button type="submit" class="btn btn-primary pull-right"><i class="glyphicon glyphicon-search"></i> Search</button>
                </form>
                @if($sending_emails->count())
                    <div class="table-responsive">
                        <table class="table table-condensed table-striped">
                            <thead>
                                <tr>
                                    <th class="text-center">#</th>
                                    <th>Email_template_id</th>
                                    <th>Member_id</th>
                                    <th>Plant_at</th>
                                    <th>Send_status</th>
                                    <th>Products</th>
                                    <th>Operate</th>
                                </tr>
                            </thead>

                            <tbody>
                                @foreach($sending_emails as $sending_email)
                                    <tr @if(2 == $sending_email->send_status) class="danger"
                                        @elseif(1 == $sending_email->send_status) class="success"
                                        @elseif(3 == $sending_email->send_status) class="warning"
                                            @endif>
                                        <td class="text-center"><strong>{{$sending_email->id}}</strong></td>

                                        <td>{{ $sending_email->template ? $sending_email->template->name : '未知模板'}}</td>
                                        <td>{{$sending_email->member->email}} <br>{{$sending_email->member->name}}</td>
                                        <td>{{$sending_email->plant_at}}</td>
                                        <td>{{ $status_map[$sending_email->send_status] }}</td>
                                        <td>
                                            @if(! empty($sending_email->products))
                                                @foreach($sending_email->products as $product)
                                                    [<a href="{{route('products.show', $product->id)}}">{{ $product->name.'|'.$product->sku }}</a>]
                                                    <br>
                                                    Price: $ {{$product->promotion_price.'/'. $product->price}}
                                                @endforeach
                                            @endif
                                        </td>
                                        <td>
                                            <button type="button" class="btn btn-xs btn-primary" onclick="send_again({{$sending_email->id}})">
                                                重新发送
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {!! $sending_emails->render() !!}
                    </div>
                @else
                    <h3 class="text-center alert alert-info">Empty!</h3>
                @endif
            </div>
        </div>
    </div>
</div>

@endsection
@push('js')
<script>
  function send_again(id) {
    $.ajax({
      type: "GET",
      dataType: "json",
      url: "/email_send/retry?id=" + id,
      success: function (result) {
        alert(result.msg);
      },
      error : function() {
        alert("网络异常！");
      }
    })
  }

  function retry_all() {
    confirm('确定要发送全部失败邮件吗？');
    $.ajax({
      type: "GET",
      dataType: "json",
      url: "/email_send/retry_all",
      success: function (result) {
        alert(result.msg);
      },
      error : function() {
        alert("网络异常！");
      }
    })
  }
</script>
@endpush