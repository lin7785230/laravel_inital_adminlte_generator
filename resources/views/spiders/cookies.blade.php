@extends('layouts.admin')

@section('content')

    <div class="container">
        <div class="col-md-10">
            <div class="panel panel-default">

                <div class="panel-heading">
                    <h1>
                        <i class="glyphicon glyphicon-edit"></i> Spider /
                        Edit Cookies
                    </h1>
                </div>

                @include('common.error')

                <div class="panel-body">
                    <form action="{{ route('spider_cookies.store') }}" method="POST" accept-charset="UTF-8">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <label>Last Modify</label>
                            <div>{{ old('last_modify', $last_modify) }}</div>
                        </div>
                        <div class="form-group">
                            <label>View Amazon</label>
                            <div><a href="https://www.amazon.com/gp/css/homepage.html/ref=nav_youraccount_ya" target="_blank">Amazon.com</a></div>
                        </div>
                        <div class="form-group">
                            <label for="cookies_content">cookies_content</label>
                            <textarea class="form-control" name="cookies_content" id="cookies_content" rows="15">{{ old('cookies_content', $cookies_content) }}</textarea>
                        </div>
                        <div class="well well-sm">
                            <button type="submit" class="btn btn-primary">Save</button>
                            <a class="btn btn-link pull-right" href="{{ route('spiders.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
                        </div>
                    </form>
                </div>
                <div class="panel-footer">
                    <p>Tips: 使用谷歌浏览器的插件 EditThisCookie，复制已经登陆的亚马逊用户账号cookies，每次抓取前操作一次，以免被亚马逊拦截</p>
                    <p>EditThisCode 插件：<a href="https://chrome.google.com/webstore/detail/editthiscookie/fngmhnnpilhplaeedifhccceomclgfbg?hl=zh-CN&utm_source=chrome-ntp-launcher">EditThisCode</a></p>
                    <p>如何复制参考：<a href="http://www.editthiscookie.com/blog/2014/03/import-export-cookies/">Export Cookies</a></p>
                </div>
            </div>
        </div>
    </div>

@endsection
