@extends('layouts.admin')

@section('content')

<div class="container">
    <div class="col-md-10">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1>Spider / Show #{{ $spider->id }}</h1>
            </div>

            <div class="panel-body">
                <div class="well well-sm">
                    <div class="row">
                        <div class="col-md-6">
                            <a class="btn btn-link" href="{{ route('spiders.index') }}"><i class="glyphicon glyphicon-backward"></i> Back</a>
                        </div>
                        <div class="col-md-6">
                             <a class="btn btn-sm btn-warning pull-right" href="{{ route('spiders.edit', $spider->id) }}">
                                <i class="glyphicon glyphicon-edit"></i> Edit
                            </a>
                        </div>
                    </div>
                </div>

                <label>Name</label>
<p>
	{{ $spider->name }}
</p> <label>Content</label>
<p>
	{{ $spider->content }}
</p> <label>Status</label>
<p>
	{{ $spider->status_name }}
</p> <label>Plant_at</label>
<p>
	{{ $spider->plant_at }}
</p>
            </div>
        </div>
    </div>
    @if($spider->status > -1)
        <div class="well well-sm">
            <div class="row">
                <div class="col-md-6">
                    <button type="button" class="btn btn-sm btn-info show-logs" onclick="showLogs()">
                        查看日志
                    </button>
                    <button type="button" style="display: none;" class="btn btn-sm btn-info disableScrollLock">
                        Disable Scroll Lock
                    </button>
                    <button type="button" class="btn btn-sm btn-info enableScrollLock">
                        Enable Scroll Lock
                    </button>
                </div>
            </div>
        </div>
        <div class="logs col-md-10 hidden">
            <div id="log">

            </div>
            <div id="scrollLock">
                <div class="row">
                    <div class="col-md-6">
                        <button type="button" style="display: none;" class="btn btn-sm btn-info disableScrollLock">
                            Disable Scroll Lock
                        </button>
                        <button type="button" class="btn btn-sm btn-info enableScrollLock">
                            Enable Scroll Lock
                        </button>
                    </div>
                </div>
            </div>
        </div>
    @endif
</div>

@endsection
@push('css')
<style>
    @import url(http://fonts.googleapis.com/css?family=Ubuntu);
    .logs{
        background-color: black;
        color: white;
        font-family: 'Ubuntu', sans-serif;
        font-size: 16px;
        line-height: 20px;
    }
    #log {
        position: relative;
    }
    #scrollLock{
        width:2px;
        height: 2px;
        overflow:visible;
    }
</style>
@endpush
@push('js')
<script>
  var pathname = "{{ route('spider_log', ['file_name' => $spider->name]) }}";
  var scrollLock = false;

  $(document).ready(function(){
    $('.disableScrollLock').click(function(){
      $("html,body").clearQueue();
      $(".disableScrollLock").hide();
      $(".enableScrollLock").show();
      scrollLock = false;
    });
    $('.enableScrollLock').click(function(){
      $("html,body").clearQueue();
      $(".enableScrollLock").hide();
      $(".disableScrollLock").show();
      scrollLock = true;
    });
  });

  function showLogs() {
    $('.logs').removeClass('hidden');
    $('.show-logs').addClass('disabled');
    readLogFile();
    setInterval(readLogFile, 5000);
  }

  function readLogFile(){
    $.get(pathname, {}, function(data) {
      data = data.replace(new RegExp("\n", "g"), "<br />");
      $("#log").html(data);

      if(scrollLock == true) {
        $('html,body').animate({scrollTop: $("#scrollLock").offset().top}, 1000)
      }
    });
  }
</script>
@endpush