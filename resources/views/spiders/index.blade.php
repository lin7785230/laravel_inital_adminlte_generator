@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="col-md-10">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1>
                    <i class="glyphicon glyphicon-align-justify"></i> Spider
                    <a class="btn btn-success pull-right" href="{{ route('spiders.create') }}"><i class="glyphicon glyphicon-plus"></i> Create</a>
                </h1>
            </div>

            <div class="panel-body">
                <form class="form-inline" action="{{route('spiders.index')}}">
                    <div class="form-group">
                        <label for="search-name">Name</label>
                        <input type="text" class="form-control" name="name" id="search-name" value="{{request('name')}}" placeholder="name">
                    </div>
                    <div class="form-group">
                        <label for="search-status">Status</label>
                        <select name="status" id="search-status" class="form-control">
                            <option value="">全部</option>
                            @foreach($status_map as $key => $value)
                                <option value="{{ $key }}" @if(''!=request('status') && request('status') == $key) selected @endif>{{ $value }}</option>
                            @endforeach
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary pull-right"><i class="glyphicon glyphicon-search"></i> Search</button>
                </form>
                <div class="table-responsive">
                @if($spiders->count())
                    <table class="table table-condensed table-striped">
                        <thead>
                            <tr>
                                <th class="text-center">#</th>
                                <th>Name</th> <th>Content</th> <th>Status</th> <th>Plant_at</th>
                                <th class="text-right">OPTIONS</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach($spiders as $spider)
                                <tr class="@if(1 == $spider->status) success @elseif (in_array($spider->status, [2, 3])) danger @endif">
                                    <td class="text-center"><strong>{{$spider->id}}</strong></td>

                                    <td>{{$spider->name}}</td>
                                    <td title="{{$spider->content}}">{{\Illuminate\Support\Str::words($spider->content, 5)}}</td>
                                    <td>{{$status_map[$spider->status]}}</td>
                                    <td>{{$spider->plant_at}}</td>
                                    
                                    <td class="text-right">
                                        <a class="btn btn-xs btn-primary" href="{{ route('spiders.show', $spider->id) }}">
                                            <i class="glyphicon glyphicon-eye-open"></i> 
                                        </a>
                                        
                                        <a class="btn btn-xs btn-warning" href="{{ route('spiders.edit', $spider->id) }}">
                                            <i class="glyphicon glyphicon-edit"></i> 
                                        </a>

                                        <form action="{{ route('spiders.destroy', $spider->id) }}" method="POST" style="display: inline;" onsubmit="return confirm('Delete? Are you sure?');">
                                            {{csrf_field()}}
                                            <input type="hidden" name="_method" value="DELETE">

                                            <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {!! $spiders->render() !!}
                @else
                    <h3 class="text-center alert alert-info">Empty!</h3>
                @endif
                </div>
            </div>
        </div>
    </div>
</div>

@endsection