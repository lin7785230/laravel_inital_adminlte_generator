@extends('layouts.admin')

@section('content')

<div class="container">
    <div class="col-md-10">
        <div class="panel panel-default">
            
            <div class="panel-heading">
                <h1>
                    <i class="glyphicon glyphicon-edit"></i> Spider /
                    @if($spider->id)
                        Edit #{{$spider->id}}
                    @else
                        Create
                    @endif
                </h1>
            </div>

            @include('common.error')

            <div class="panel-body">
                @if($spider->id)
                    <form action="{{ route('spiders.update', $spider->id) }}" method="POST" accept-charset="UTF-8">
                        <input type="hidden" name="_method" value="PUT">
                @else
                    <form action="{{ route('spiders.store') }}" method="POST" accept-charset="UTF-8">
                @endif

                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    
                <div class="form-group">
                	<label for="name-field">Name</label>
                	<input class="form-control" type="text" name="name" id="name-field" value="{{ old('name', $spider->name ) }}" />
                </div>
                @if($spider->id)
                    <div class="form-group">
                        <label for="content-field">Content</label>
                        <input class="form-control" type="text" name="content" id="content-field" value="{{ old('content', $spider->content ) }}" />
                    </div>
                    <div class="form-group">
                        <label for="status">状态：</label>
                        <select class="form-control" name="status" id="status">
                            @foreach($status_map as $key=>$value)
                            <option @if($key == $spider->status) selected @endif value="{{ $key }}">{{ $value }}</option>
                            @endforeach
                        </select>
                    </div>
                @else
                    <div class="form-group">
                        <label for="sku-field">Asin</label>
                        <input type="text" class="form-control" name="sku" id="sku-field" value="">
                    </div>
                    <div class="form-group">
                        <label for="spider_name">爬虫</label>
                        <select class="form-control" name="spider_name" id="spider_name">
                            <option selected value="amazon_reviewer">Amazon Reviewer Email</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="sort_by">排序</label>
                        <select class="form-control" name="sort_by" id="sort_by">
                            <option selected value="recent">recent</option>
                            <option value="helpful">helpful</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="filter_by_star">评价等级</label>
                        <select class="form-control" name="filter_by_star" id="filter_by_star">
                            <option selected value="all_stars">all_stars</option>
                            <option value="five_star">five_star</option>
                            <option value="four_star">four_star</option>
                            <option value="three_star">three_star</option>
                            <option value="two_star">two_star</option>
                            <option value="one_star">one_star</option>
                        </select>
                    </div>
                            <div class="form-group">
                                <label for="min_page_no">开始页码</label>
                                <input class="form-control" type="text" name="min_page_no" id="min_page_no-field" value="{{ $spider->min_page_no ? old('min_page_no', $spider->min_page_no) : '1' }}" />
                            </div>
                            <div class="form-group">
                                <label for="max_page_no">结束页码</label>
                                <input class="form-control" type="text" name="max_page_no" id="max_page_no-field" value="{{ $spider->max_page_no ? old('max_page_no', $spider->max_page_no ) : '5' }}" />
                            </div>
                @endif
                <div class="form-group">
                    <label for="datetimepicker1" class="control-label">开始时间</label>
                    <input type='text' class="form-control" name="plant_at" id='datetimepicker1' value="{{ old('plant_at', $spider->plant_at ) }}"/>
                </div>
                        <div class="well well-sm">
                        <button type="submit" class="btn btn-primary">Save</button>
                        <a class="btn btn-link pull-right" href="{{ route('spiders.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
@section('css')
    <link href="https://cdn.bootcss.com/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
@endsection
@push('js')
<script src="https://cdn.bootcss.com/moment.js/2.20.1/moment.min.js"></script>
<script src="https://cdn.bootcss.com/moment.js/2.20.1/locale/zh-cn.js"></script>
<script src="https://cdn.bootcss.com/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
<script>
  $(function(){
    $("#datetimepicker1").datetimepicker();
  });

</script>
@endpush