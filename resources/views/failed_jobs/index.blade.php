@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="col-md-10">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h1>
                        <i class="glyphicon glyphicon-align-justify"></i> FailedJobs
                        <a class="btn btn-success pull-right" href="{{ route('failed_jobs.redo', ['queue' => request('queue')]) }}" target="_blank"><i
                                    class="glyphicon glyphicon-plus"></i> 全部重做</a>
                        <a class="btn btn-danger pull-right" onclick="return confirm('Delete All? Are you sure?');" href="{{ route('failed_jobs.flush', ['queue' => request('queue')]) }}" target="_blank"><i
                                    class="glyphicon glyphicon-trash"></i> 全部清空</a>
                    </h1>
                </div>

                <div class="panel-body ">
                    <div class="table-responsive">
                    @if($jobs->count())
                        <table class="table table-condensed table-striped">
                            <thead>
                            <tr>
                                <th class="text-center">#</th>
                                <th>Connection</th>
                                <th>Queue</th>
                                <th>Failed_at</th>
                                <th class="text-right">OPTIONS</th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($jobs as $job)
                                <tr>
                                    <td class="text-center"><strong>{{$job->id}}</strong></td>

                                    <td>{{$job->connection}}</td>
                                    <td>{{$job->queue}}</td>
                                    <td>{{$job->failed_at}}</td>

                                    <td class="text-right">
                                        <a class="btn btn-xs btn-primary"
                                           href="{{ route('failed_jobs.show', $job->id) }}">
                                            <i class="glyphicon glyphicon-eye-open"></i>
                                        </a>

                                        <a class="btn btn-xs btn-warning"
                                           href="{{ route('failed_jobs.redo', $job->id) }}">
                                            <i class="glyphicon glyphicon-edit">重做</i>
                                        </a>

                                        <form action="{{ route('failed_jobs.destroy', $job->id) }}"
                                              method="POST" style="display: inline;"
                                              onsubmit="return confirm('Delete? Are you sure?');">
                                            {{csrf_field()}}
                                            <input type="hidden" name="_method" value="DELETE">

                                            <button type="submit" class="btn btn-xs btn-danger"><i
                                                        class="glyphicon glyphicon-trash"></i></button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {!! $jobs->render() !!}
                    @else
                        <h3 class="text-center alert alert-info">Empty!</h3>
                    @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection