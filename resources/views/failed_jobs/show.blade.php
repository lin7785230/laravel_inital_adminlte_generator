@extends('layouts.admin')

@section('content')

    <div class="container">
        <div class="col-md-10">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h1>FailedJobs / Show #{{ $job->id }}</h1>
                </div>

                <div class="panel-body">
                    <div class="well well-sm">
                        <div class="row">
                            <div class="col-md-6">
                                <a class="btn btn-link" href="{{ route('failed_jobs.index') }}"><i
                                            class="glyphicon glyphicon-backward"></i> Back</a>
                            </div>
                        </div>
                    </div>

                    <label>Connection</label>
                    <p>
                        {{ $job->connection }}
                    </p> <label>Queue</label>
                    <p>
                        {{ old('queue', $job->queue ) }}
                    </p>
                    <label>Failed_at</label>
                    <p>
                        {{ old('failed_at', $job->failed_at ) }}
                    </p>
                    <label>Payload</label>
                    <p>
                        {!! old('payload', $job->payload ) !!}
                    </p>
                    <label>Exception</label>
                    <p>
                        {!! old('exception', $job->exception ) !!}
                    </p>
                </div>
            </div>
        </div>
    </div>

@endsection
