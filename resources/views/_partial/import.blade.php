<button class="btn btn-primary pull-right margin-r-5" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
    导入
</button>
<div class="collapse" id="collapseExample">
    <div class="well">
        <form enctype="multipart/form-data" class="form-inline" id="fileupload" method="POST" action="{{ $action }}">
            {{csrf_field()}}
            <div class="form-group">
                <input type="file" name="file" id="exampleInputFile">
            </div>
            <div class="form-group">
                <a href="{{ $template_url }}" target="_blank">模板</a>
            </div>
            <button type="submit" class="btn btn-default">开始导入</button>
        </form>

    </div>
</div>
