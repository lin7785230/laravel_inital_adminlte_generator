@extends('layouts.admin')

@section('content')

<div class="container">
    <div class="col-md-10">
        <div class="panel panel-default">
            
            <div class="panel-heading">
                <h1>
                    <i class="glyphicon glyphicon-edit"></i> SpiderAccount /
                    @if($spider_account->id)
                        Edit #{{$spider_account->id}}
                    @else
                        Create
                    @endif
                </h1>
            </div>

            @include('common.error')

            <div class="panel-body">
                @if($spider_account->id)
                    <form action="{{ route('spider_accounts.update', $spider_account->id) }}" method="POST" accept-charset="UTF-8">
                        <input type="hidden" name="_method" value="PUT">
                @else
                    <form action="{{ route('spider_accounts.store') }}" method="POST" accept-charset="UTF-8">
                @endif

                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    
                <div class="form-group">
                	<label for="name-field">Name</label>
                	<input class="form-control" type="text" name="name" id="name-field" value="{{ old('name', $spider_account->name ) }}" />
                </div> 
                <div class="form-group">
                	<label for="password-field">Password</label>
                	<input class="form-control" type="text" name="password" id="password-field" value="{{ old('password', $spider_account->password ) }}" />
                </div>
                <div class="form-group">
                    <label for="status-field">Status</label>
                    <select class="form-control" name="status" id="status-field">
                        <option selected value="1">正常</option>
                        <option selected value="0">Detected</option>
                    </select>
                </div>
                <div class="form-group">
                	<label for="type-field">Type</label>
                    <select class="form-control" name="type" id="type-field">
                        <option selected value="amazon">Amazon Account</option>
                    </select>
                </div> 
                <div class="form-group">
                	<label for="content-field">Content</label>
                	<textarea class="form-control" name="content" id="content-field" rows="15">{{ old('content', $spider_account->content ) }}</textarea>
                </div>

                    <div class="well well-sm">
                        <button type="submit" class="btn btn-primary">Save</button>
                        <a class="btn btn-link pull-right" href="{{ route('spider_accounts.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
                    </div>
                </form>
            </div>
            <div class="panel-footer">
                <p>Tips: 使用谷歌浏览器的插件 EditThisCookie，复制已经登陆的亚马逊用户账号cookies，每次抓取前操作一次，以免被亚马逊拦截</p>
                <p>EditThisCode 插件：<a href="https://chrome.google.com/webstore/detail/editthiscookie/fngmhnnpilhplaeedifhccceomclgfbg?hl=zh-CN&utm_source=chrome-ntp-launcher">EditThisCode</a></p>
                <p>如何复制参考：<a href="http://www.editthiscookie.com/blog/2014/03/import-export-cookies/">Export Cookies</a></p>
            </div>
        </div>
    </div>
</div>

@endsection