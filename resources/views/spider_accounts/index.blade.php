@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="col-md-10">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1>
                    <i class="glyphicon glyphicon-align-justify"></i> SpiderAccount
                    <a class="btn btn-success pull-right" href="{{ route('spider_accounts.create') }}"><i class="glyphicon glyphicon-plus"></i> Create</a>
                </h1>
            </div>

            <div class="panel-body">
                <div class="table-responsive">
                @if($spider_accounts->count())
                    <table class="table table-condensed table-striped">
                        <thead>
                            <tr>
                                <th class="text-center">#</th>
                                <th>Name</th> <th>Password</th> <th>Status</th> <th>Type</th> <th>UpdatedAt</th>
                                <th class="text-right">OPTIONS</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach($spider_accounts as $spider_account)
                                <tr>
                                    <td class="text-center"><strong>{{$spider_account->id}}</strong></td>

                                    <td>{{$spider_account->name}}</td> <td>{{$spider_account->password}}</td> <td>{{$spider_account->status ? '正常' : 'Detected'}}</td> <td>{{$spider_account->type}}</td> <td>{{$spider_account->updated_at}}</td>
                                    
                                    <td class="text-right">
                                        <a class="btn btn-xs btn-primary" href="{{ route('spider_accounts.show', $spider_account->id) }}">
                                            <i class="glyphicon glyphicon-eye-open"></i> 
                                        </a>
                                        
                                        <a class="btn btn-xs btn-warning" href="{{ route('spider_accounts.edit', $spider_account->id) }}">
                                            <i class="glyphicon glyphicon-edit"></i> 
                                        </a>

                                        <form action="{{ route('spider_accounts.destroy', $spider_account->id) }}" method="POST" style="display: inline;" onsubmit="return confirm('Delete? Are you sure?');">
                                            {{csrf_field()}}
                                            <input type="hidden" name="_method" value="DELETE">

                                            <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {!! $spider_accounts->render() !!}
                    </div>
                @else
                    <h3 class="text-center alert alert-info">Empty!</h3>
                @endif
            </div>
        </div>
    </div>
</div>

@endsection