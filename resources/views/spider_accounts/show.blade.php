@extends('layouts.admin')

@section('content')

<div class="container">
    <div class="col-md-10">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1>SpiderAccount / Show #{{ $spider_account->id }}</h1>
            </div>

            <div class="panel-body">
                <div class="well well-sm">
                    <div class="row">
                        <div class="col-md-6">
                            <a class="btn btn-link" href="{{ route('spider_accounts.index') }}"><i class="glyphicon glyphicon-backward"></i> Back</a>
                        </div>
                        <div class="col-md-6">
                             <a class="btn btn-sm btn-warning pull-right" href="{{ route('spider_accounts.edit', $spider_account->id) }}">
                                <i class="glyphicon glyphicon-edit"></i> Edit
                            </a>
                        </div>
                    </div>
                </div>

                <label>Name</label>
<p>
	{{ $spider_account->name }}
</p> <label>Password</label>
<p>
	{{ $spider_account->password }}
</p> <label>Status</label>
<p>
	{{ $spider_account->status ? '正常' : 'Detected' }}
</p> <label>Type</label>
<p>
	{{ $spider_account->type }}
</p><label>UpdatedAt</label>
<p>
	{{ $spider_account->updated_at }}
</p> <label>Content</label>
<p>
    <pre style="height: 200px">
	{{ $spider_account->content }}
    </pre>
</p>
            </div>
        </div>
    </div>
</div>

@endsection
