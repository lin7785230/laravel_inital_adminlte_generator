@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="col-md-10">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1>
                    <i class="glyphicon glyphicon-align-justify"></i> CrawlUserEmail
                    <a class="btn btn-success pull-right" href="{{ route('crawl_user_emails.create') }}"><i class="glyphicon glyphicon-plus"></i> Create</a>
                </h1>
            </div>

            <div class="panel-body">
                <form class="form-inline" action="{{route('crawl_user_emails.index')}}">
                    <div class="form-group">
                        <label for="search-sku">Asin</label>
                        <input type="text" class="form-control" name="sku" id="search-sku" value="{{ request('sku') }}" placeholder="asin">
                    </div>
                    <button type="submit" class="btn btn-primary pull-right"><i class="glyphicon glyphicon-search"></i> Search</button>
                </form>
                <div class="table-responsive">
                @if($crawl_user_emails->count())
                    <table class="table table-condensed table-striped">
                        <thead>
                            <tr>
                                <th class="text-center">#</th>
                                <th>Sku</th> <th>User_id</th> <th>User_name</th> <th>Email</th><th>Created_at</th>
                                <th class="text-right">OPTIONS</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach($crawl_user_emails as $crawl_user_email)
                                <tr>
                                    <td class="text-center"><strong>{{$crawl_user_email->id}}</strong></td>
                                    <td>{{$crawl_user_email->sku}}</td>
                                    <td>{{$crawl_user_email->user_id}}</td>
                                    <td>{{$crawl_user_email->user_name}}</td>
                                    <td>{{$crawl_user_email->email}}</td>
                                    <td>{{$crawl_user_email->created_at}}</td>
                                    <td class="text-right">
                                        <a class="btn btn-xs btn-info" href="{{ route('crawl_user_emails.create', ['id' => $crawl_user_email->id]) }}">
                                            发送邮件
                                        </a>
                                        <form action="{{ route('crawl_user_emails.destroy', $crawl_user_email->id) }}" method="POST" style="display: inline;" onsubmit="return confirm('Delete? Are you sure?');">
                                            {{csrf_field()}}
                                            <input type="hidden" name="_method" value="DELETE">

                                            <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {!! $crawl_user_emails->render() !!}
                @else
                    <h3 class="text-center alert alert-info">Empty!</h3>
                @endif
                </div>
            </div>
        </div>
    </div>
</div>

@endsection