@extends('layouts.admin')

@section('content')

<div class="container">
    <div class="col-md-10">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1>CrawlUserEmail / Show #{{ $crawl_user_email->id }}</h1>
            </div>

            <div class="panel-body">
                <div class="well well-sm">
                    <div class="row">
                        <div class="col-md-6">
                            <a class="btn btn-link" href="{{ route('crawl_user_emails.index') }}"><i class="glyphicon glyphicon-backward"></i> Back</a>
                        </div>
                        <div class="col-md-6">
                             <a class="btn btn-sm btn-warning pull-right" href="{{ route('crawl_user_emails.edit', $crawl_user_email->id) }}">
                                <i class="glyphicon glyphicon-edit"></i> Edit
                            </a>
                        </div>
                    </div>
                </div>

                <label>Sku</label>
<p>
	{{ $crawl_user_email->sku }}
</p> <label>User_id</label>
<p>
	{{ $crawl_user_email->user_id }}
</p> <label>User_name</label>
<p>
	{{ $crawl_user_email->user_name }}
</p> <label>Email</label>
<p>
	{{ $crawl_user_email->email }}
</p>
            </div>
        </div>
    </div>
</div>

@endsection
