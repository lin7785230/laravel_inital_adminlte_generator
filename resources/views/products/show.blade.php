@extends('layouts.admin')

@section('content')

<div class="container">
    <div class="col-md-10">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1>Product / Show #{{ $product->id }}</h1>
            </div>

            <div class="panel-body">
                <div class="well well-sm">
                    <div class="row">
                        <div class="col-md-6">
                            <a class="btn btn-link" href="{{ route('products.index') }}"><i class="glyphicon glyphicon-backward"></i> Back</a>
                        </div>
                        <div class="col-md-6">
                             <a class="btn btn-sm btn-warning pull-right" href="{{ route('products.edit', $product->id) }}">
                                <i class="glyphicon glyphicon-edit"></i> Edit
                            </a>
                        </div>
                    </div>
                </div>

                <label>SKU</label>
<p>
	{{ $product->sku }}
</p>
                <label>Name</label>
                <p>
                    {{ $product->name }}
                </p>
                <label>Desc</label>
<p>
	{{ $product->desc }}
</p> <label>Price</label>
<p>
	{{ $product->price }}
</p> <label>Promotion_price</label>
<p>
	{{ $product->promotion_price }}
</p> <label>Picture_url</label>
<p>
    <img src="{{ $product->picture_url }}">
</p> <label>Link_url</label>
<p>
	{{ $product->link_url }}
</p> <label>Notes</label>
<p>
	{{ $product->notes }}
</p>
            </div>
        </div>
    </div>
</div>

@endsection
