@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="col-md-10">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1>
                    <i class="glyphicon glyphicon-align-justify"></i> Product
                    <a class="btn btn-success pull-right" href="{{ route('products.create') }}"><i class="glyphicon glyphicon-plus"></i> Create</a>
                </h1>
                @component('_partial.import', ['template_url' => '/excel/products_template.xlsx', 'action' => route('products.store', ['excel'=>1])])
                @endcomponent
            </div>

            <div class="panel-body">
                <form class="form-inline" action="{{route('products.index')}}">
                    <div class="form-group">
                        <label for="search-sku">Asin</label>
                        <input type="text" class="form-control" name="sku" id="search-sku" value="{{ request('sku') }}" placeholder="sku">
                    </div>
                    <div class="form-group">
                        <label for="search-name">Name</label>
                        <input type="text" class="form-control" name="name" id="search-name" value="{{ request('name') }}" placeholder="name">
                    </div>
                    <button type="submit" class="btn btn-primary pull-right"><i class="glyphicon glyphicon-search"></i> Search</button>
                </form>
                @if($products->count())
                    <div class="table-responsive">
                        <table class="table table-condensed table-striped">
                            <thead>
                            <tr>
                                <th class="text-center">#</th>
                                <th>SKU</th>
                                <th>EmailCount</th>
                                <th>Name</th>
                                <th>Desc</th>
                                <th>Price</th>
                                <th>Promotion_price</th>
                                <th>Picture_url</th>
                                <th>Link_url</th>
                                <th>Notes</th>
                                <th class="text-right">OPTIONS</th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($products as $product)
                                <tr>
                                    <td class="text-center"><strong>{{$product->id}}</strong></td>

                                    <td>{{$product->sku}}</td>
                                    <td>{{$product->emails_count}}</td>
                                    <td>{{$product->name}}</td>
                                    <td>{{ \Illuminate\Support\Str::words($product->desc, 20) }}</td>
                                    <td>${{$product->price}}</td>
                                    <td>${{$product->promotion_price}}</td>
                                    <td><img src="{{$product->picture_url}}" style="height: 50px"></td>
                                    <td><a href="{{$product->link_url}}" target="_blank">商品链接</a></td>
                                    <td>{{ \Illuminate\Support\Str::words($product->notes, 20) }}</td>

                                    <td class="text-right">
                                        <a class="btn btn-xs btn-primary" href="{{ route('products.show', $product->id) }}">
                                            <i class="glyphicon glyphicon-eye-open"></i>
                                        </a>

                                        <a class="btn btn-xs btn-warning" href="{{ route('products.edit', $product->id) }}">
                                            <i class="glyphicon glyphicon-edit"></i>
                                        </a>

                                        <form action="{{ route('products.destroy', $product->id) }}" method="POST" style="display: inline;" onsubmit="return confirm('Delete? Are you sure?');">
                                            {{csrf_field()}}
                                            <input type="hidden" name="_method" value="DELETE">

                                            <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {!! $products->render() !!}
                    </div>
                @else
                    <h3 class="text-center alert alert-info">Empty!</h3>
                @endif
            </div>
        </div>
    </div>
</div>

@endsection