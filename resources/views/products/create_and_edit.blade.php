@extends('layouts.admin')

@section('content')

<div class="container">
    <div class="col-md-10">
        <div class="panel panel-default">
            
            <div class="panel-heading">
                <h1>
                    <i class="glyphicon glyphicon-edit"></i> Product /
                    @if($product->id)
                        Edit #{{$product->id}}
                    @else
                        Create
                    @endif
                </h1>
            </div>

            @include('common.error')

            <div class="panel-body">
                @if($product->id)
                    <form action="{{ route('products.update', $product->id) }}" method="POST" accept-charset="UTF-8">
                        <input type="hidden" name="_method" value="PUT">
                @else
                    <form action="{{ route('products.store') }}" method="POST" accept-charset="UTF-8">
                @endif

                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                	<label for="sku-field">SKU</label>
                	<input class="form-control" type="text" name="sku" id="sku-field" value="{{ old('sku', $product->name ) }}" />
                </div>
                <div class="form-group">
                	<label for="name-field">Name</label>
                	<input class="form-control" type="text" name="name" id="name-field" value="{{ old('name', $product->name ) }}" />
                </div>
                <div class="form-group">
                	<label for="desc-field">Desc</label>
                	<input class="form-control" type="text" name="desc" id="desc-field" value="{{ old('desc', $product->desc ) }}" />
                </div> 
                <div class="form-group">
                    <label for="price-field">Price</label>
                    <input class="form-control" type="text" name="price" id="price-field" value="{{ old('price', $product->price ) }}" />
                </div> 
                <div class="form-group">
                    <label for="promotion_price-field">Promotion_price</label>
                    <input class="form-control" type="text" name="promotion_price" id="promotion_price-field" value="{{ old('promotion_price', $product->promotion_price ) }}" />
                </div> 
                <div class="form-group">
                	<label for="picture_url-field">Picture_url</label>
                	<input class="form-control" type="text" name="picture_url" id="picture_url-field" value="{{ old('picture_url', $product->picture_url ) }}" />
                </div> 
                <div class="form-group">
                	<label for="link_url-field">Link_url</label>
                	<input class="form-control" type="text" name="link_url" id="link_url-field" value="{{ old('link_url', $product->link_url ) }}" />
                </div> 
                <div class="form-group">
                	<label for="notes-field">Notes</label>
                    <textarea class="form-control" name="notes" id="notes-field" cols="30" rows="3">{{ old('notes', $product->notes ) }}</textarea>
                </div>

                    <div class="well well-sm">
                        <button type="submit" class="btn btn-primary">Save</button>
                        <a class="btn btn-link pull-right" href="{{ route('products.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection