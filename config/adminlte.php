<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Title
    |--------------------------------------------------------------------------
    |
    | The default title of your admin panel, this goes into the title tag
    | of your page. You can override it per page with the title section.
    | You can optionally also specify a title prefix and/or postfix.
    |
    */

    'title' => 'YoYi',

    'title_prefix' => '',

    'title_postfix' => '',

    /*
    |--------------------------------------------------------------------------
    | Logo
    |--------------------------------------------------------------------------
    |
    | This logo is displayed at the upper left corner of your admin panel.
    | You can use basic HTML here if you want. The logo has also a mini
    | variant, used for the mini side bar. Make it 3 letters or so
    |
    */

    'logo' => 'Will\'s Amazon',

    'logo_mini' => 'Will',

    /*
    |--------------------------------------------------------------------------
    | Skin Color
    |--------------------------------------------------------------------------
    |
    | Choose a skin color for your admin panel. The available skin colors:
    | blue, black, purple, yellow, red, and green. Each skin also has a
    | ligth variant: blue-light, purple-light, purple-light, etc.
    |
    */

    'skin' => 'blue-light',

    /*
    |--------------------------------------------------------------------------
    | Layout 布局，菜单位置
    |--------------------------------------------------------------------------
    |
    | Choose a layout for your admin panel. The available layout options:
    | null, 'boxed', 'fixed', 'top-nav'. null is the default, top-nav
    | removes the sidebar and places your menu in the top navbar
    |
    */

    'layout' => 'fixed',

    /*
    |--------------------------------------------------------------------------
    | Collapse Sidebar 是否自动展开
    |--------------------------------------------------------------------------
    |
    | Here we choose and option to be able to start with a collapsed side
    | bar. To adjust your sidebar layout simply set this  either true
    | this is compatible with layouts except top-nav layout option
    |
    */

    'collapse_sidebar' => false,

    /*
    |--------------------------------------------------------------------------
    | URLs
    |--------------------------------------------------------------------------
    |
    | Register here your dashboard, logout, login and register URLs. The
    | logout URL automatically sends a POST request in Laravel 5.3 or higher.
    | You can set the request to a GET or POST with logout_method.
    | Set register_url to null if you don't want a register link.
    |
    */

    'dashboard_url' => 'home',

    'logout_url' => 'logout',

    'logout_method' => 'post',

    'login_url' => 'login',

    'register_url' => '',

    /*
    |--------------------------------------------------------------------------
    | Menu Items 菜单设置，可以按照权限过滤内容
    |--------------------------------------------------------------------------
    |
    | Specify your menu items to display in the left sidebar. Each menu item
    | should have a text and and a URL. You can also specify an icon from
    | Font Awesome. A string instead of an array represents a header in sidebar
    | layout. The 'can' is a filter on Laravel's built in Gate functionality.
    |
    */

    'menu' => [
        /*'管理员管理',
        [
            'text' => '管理员列表',
            'url'  => 'users',
            'icon' => 'user',
        ],
        [
            'text' => 'Failed Jobs',
            'url'  => 'failed_jobs',
            'icon' => 'job',
        ],
        [
            'text' => '操作日志',
            'url'  => 'logs',
            'icon' => 'lock',
        ],*/
        '会员管理',
        [
            'text' => '会员列表',
            'url'  => 'members',
            'icon' => 'user',
        ],
        [
            'text' => '标签tags',
            'url'  => 'tags',
            'icon' => 'tags',
        ],
        '商品管理',
        [
            'text'       => '商品列表',
            'url'   => 'products',
            'icon' => 'product-hunt',
        ],
        '爬虫管理',
        [
            'text'       => '任务列表',
            'url'   => 'spiders',
            'icon' => 'tasks',
        ],
        [
            'text'       => '新增任务',
            'url'   => 'spiders/create',
            'icon' => 'plus-square',
        ],
        [
            'text'       => '爬取结果',
            'url'   => 'crawl_user_emails',
            'icon' => 'bug',
        ],
        [
            'text'       => '买家账号',
            'url'   => 'spider_accounts',
            'icon' => 'user-secret',
        ],
        [
            'text'       => '更新cookies',
            'url'   => 'spider_cookies',
            'icon' => 'cogs',
        ],
        [
            'text' => 'Failed Jobs',
            'url'  => 'failed_jobs',
            'icon' => 'exclamation',
        ],
        '邮件管理',
        [
            'text' => '邮件列表',
            'icon'        => 'envelope-open-o',
            'url'  => 'sending_emails',
        ],
        [
            'text'        => '邮件模板',
            'url'         => 'email_templates',
            'icon'        => 'envelope'
        ],
        [
            'text'        => '邮件类型',
            'url'         => 'email_types',
            'icon'        => 'file'
        ],
        'AdminOperate',
        [
            'text' => '运行日志',
            'url'  => 'logs',
            'icon' => 'lock',
        ]
    ],

    /*
    |--------------------------------------------------------------------------
    | Menu Filters
    |--------------------------------------------------------------------------
    |
    | Choose what filters you want to include for rendering the menu.
    | You can add your own filters to this array after you've created them.
    | You can comment out the GateFilter if you don't want to use Laravel's
    | built in Gate functionality
    |
    */

    'filters' => [
        JeroenNoten\LaravelAdminLte\Menu\Filters\HrefFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ActiveFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\SubmenuFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ClassesFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\GateFilter::class,
    ],

    /*
    |--------------------------------------------------------------------------
    | Plugins Initialization
    |--------------------------------------------------------------------------
    |
    | Choose which JavaScript plugins should be included. At this moment,
    | only DataTables is supported as a plugin. Set the value to true
    | to include the JavaScript file from a CDN via a script tag.
    |
    */

    'plugins' => [
        'datatables' => true,
        'select2'    => true,
    ],
];
