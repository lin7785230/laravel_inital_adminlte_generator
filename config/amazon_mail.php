<?php

return [
    'home_page' => 'https://www.artisanates.com',
    'facebook' => '',
    'twitter' => '',
    'pinterest' => null,
    // 110*48
    'header_logo' => 'https://images-na.ssl-images-amazon.com/images/G/01/e-mail/logos/amzn_logo_110x48.png',
    // 126 * 24
    'footer_logo' => 'http://g-ecx.images-amazon.com/images/G/01/us-mail/navAmazonLogoFooter.gif',
    'notes' => 'We hope you found this message to be useful. However, if you\'d rather not receive future e-mails of this sort from Us, please email us!',
    'spider_root' => env('SPIDER_HOME', '/var/www/0-python/tutorial/'),
    'spider_cookies_file' => env('SPIDER_COOKIES_FILE', 'cookies.json')
];
