<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailTemplatesTable extends Migration 
{
	public function up()
	{
		Schema::create('email_templates', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('content');
            $table->integer('type_id');
            $table->timestamps();
            $table->softDeletes();
            
            $table->index('type_id');
        });
	}

	public function down()
	{
		Schema::drop('email_templates');
	}
}
