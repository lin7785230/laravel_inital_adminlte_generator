<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpiderAccountsTable extends Migration 
{
	public function up()
	{
		Schema::create('spider_accounts', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50);
            $table->string('password');
            $table->tinyInteger('status')->default(1)->comment('账号状态：0 不可使用，1 正常');
            $table->string('type', 50)->default('amazon');
            $table->text('content')->nullable();
            $table->timestamps();
        });
	}

	public function down()
	{
		Schema::drop('spider_accounts');
	}
}
