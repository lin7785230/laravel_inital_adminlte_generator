<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTagsTable extends Migration 
{
	public function up()
	{
		Schema::create('tags', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });
		
		Schema::create('member_tag', function(Blueprint $table)
		{
		    $table->integer('member_id');
            $table->integer('tag_id');
            $table->index(['member_id', 'tag_id']);
            $table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('tags');
        Schema::drop('member_tag');
	}
}
