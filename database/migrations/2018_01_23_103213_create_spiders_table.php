<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpidersTable extends Migration 
{
	public function up()
	{
		Schema::create('spiders', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('content');
            $table->tinyInteger('status')->default(0);
            $table->timestamp('plant_at');
            $table->timestamps();
        });
	}

	public function down()
	{
		Schema::drop('spiders');
	}
}
