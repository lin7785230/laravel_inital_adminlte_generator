<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration 
{
	public function up()
	{
		Schema::create('products', function(Blueprint $table) {
            $table->increments('id');
            $table->string('sku')->unique()->comment('商品唯一标识');
            $table->string('name')->comment('商品名称');
            $table->string('desc')->comment('商品描述');
            $table->double('price')->comment('划线价格');
            $table->double('promotion_price')->comment('促销价格');
            $table->string('picture_url')->comment('商品图片地址');
            $table->string('link_url')->comment('商品链接地址');
            $table->string('notes')->comment('其他备注');
            $table->timestamps();
            $table->softDeletes();
        });
	}

	public function down()
	{
		Schema::drop('products');
	}
}
