<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCrawlUserEmailsTable extends Migration 
{
	public function up()
	{
		Schema::create('crawl_user_emails', function(Blueprint $table) {
            $table->increments('id');
            $table->string('sku');
            $table->integer('user_id');
            $table->string('user_name');
            $table->string('email');
            $table->timestamps();
        });
	}

	public function down()
	{
		Schema::drop('crawl_user_emails');
	}
}
