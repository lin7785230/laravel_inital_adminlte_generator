<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSendingEmailsTable extends Migration 
{
	public function up()
	{
		Schema::create('sending_emails', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('email_template_id');
            $table->integer('member_id');
            $table->tinyInteger('send_status')->unsigned()->default(0)->comment('0：等待发送，1：已经发送');
            $table->timestamp('send_at')->comment('实际发送时间');
            $table->timestamp('plant_at')->comment('计划发送时间');
            $table->timestamps();

            $table->index('email_template_id');
            $table->index('member_id');
        });
	}

	public function down()
	{
		Schema::drop('sending_emails');
	}
}
