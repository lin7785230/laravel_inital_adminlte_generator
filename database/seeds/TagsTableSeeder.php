<?php

use Illuminate\Database\Seeder;
use App\Models\Tag;

class TagsTableSeeder extends Seeder
{
    public function run()
    {
        $tags = factory(Tag::class)->times(50)->make()->each(function ($tag, $index) {
            if ($index == 0) {
                // $tag->field = 'value';
            }
        });

        Tag::insert($tags->toArray());
    }

}

