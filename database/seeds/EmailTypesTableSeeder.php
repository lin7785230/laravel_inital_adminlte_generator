<?php

use Illuminate\Database\Seeder;
use App\Models\EmailType;

class EmailTypesTableSeeder extends Seeder
{
    public function run()
    {
        $email_types = factory(EmailType::class)->times(50)->make()->each(function ($email_type, $index) {
            if ($index == 0) {
                // $email_type->field = 'value';
            }
        });

        EmailType::insert($email_types->toArray());
    }

}

