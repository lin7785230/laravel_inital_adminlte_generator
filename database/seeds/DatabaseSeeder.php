<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
		$this->call(SpiderAccountsTableSeeder::class);
		$this->call(CrawlUserEmailsTableSeeder::class);
		$this->call(SpidersTableSeeder::class);
		$this->call(ProductsTableSeeder::class);
		$this->call(TagsTableSeeder::class);
		$this->call(EmailTypesTableSeeder::class);
		$this->call(SendingEmailsTableSeeder::class);
		$this->call(EmailTemplatesTableSeeder::class);
		$this->call(MembersTableSeeder::class);
    }
}
