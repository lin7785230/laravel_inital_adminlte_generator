<?php

use Illuminate\Database\Seeder;
use App\Models\Spider;

class SpidersTableSeeder extends Seeder
{
    public function run()
    {
        $spiders = factory(Spider::class)->times(50)->make()->each(function ($spider, $index) {
            if ($index == 0) {
                // $spider->field = 'value';
            }
        });

        Spider::insert($spiders->toArray());
    }

}

