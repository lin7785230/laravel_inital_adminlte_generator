<?php

use Illuminate\Database\Seeder;
use App\Models\CrawlUserEmail;

class CrawlUserEmailsTableSeeder extends Seeder
{
    public function run()
    {
        $crawl_user_emails = factory(CrawlUserEmail::class)->times(50)->make()->each(function ($crawl_user_email, $index) {
            if ($index == 0) {
                // $crawl_user_email->field = 'value';
            }
        });

        CrawlUserEmail::insert($crawl_user_emails->toArray());
    }

}

