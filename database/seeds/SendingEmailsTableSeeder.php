<?php

use Illuminate\Database\Seeder;
use App\Models\SendingEmail;

class SendingEmailsTableSeeder extends Seeder
{
    public function run()
    {
        $sending_emails = factory(SendingEmail::class)->times(50)->make()->each(function ($sending_email, $index) {
            if ($index == 0) {
                // $sending_email->field = 'value';
            }
        });

        SendingEmail::insert($sending_emails->toArray());
    }

}

