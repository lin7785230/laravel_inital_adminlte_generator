<?php

use Illuminate\Database\Seeder;
use App\Models\SpiderAccount;

class SpiderAccountsTableSeeder extends Seeder
{
    public function run()
    {
        $spider_accounts = factory(SpiderAccount::class)->times(50)->make()->each(function ($spider_account, $index) {
            if ($index == 0) {
                // $spider_account->field = 'value';
            }
        });

        SpiderAccount::insert($spider_accounts->toArray());
    }

}

