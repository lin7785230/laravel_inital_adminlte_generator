<?php

use Illuminate\Database\Seeder;
use App\Models\EmailTemplate;

class EmailTemplatesTableSeeder extends Seeder
{
    public function run()
    {
        $email_templates = factory(EmailTemplate::class)->times(50)->make()->each(function ($email_template, $index) {
            if ($index == 0) {
                // $email_template->field = 'value';
            }
        });

        EmailTemplate::insert($email_templates->toArray());
    }

}

