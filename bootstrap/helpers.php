<?php
if (!function_exists('getSql')) {
    function getSql ()
    {
        DB::listen(function ($sql) {
            dump($sql);
            $singleSql = $sql->sql;
            if ($sql->bindings) {
                foreach ($sql->bindings as $replace) {
                    $value = is_numeric($replace) ? $replace : "'" . $replace . "'";
                    $singleSql = preg_replace('/\?/', $value, $singleSql, 1);
                }
                dump($singleSql);
            } else {
                dump($singleSql);
            }
        });
    }
}

function emailLimitCacheKey($plantAt = '')
{
    if (empty($plantAt)){
        $plantAt = now();
    }

    $date = \Illuminate\Support\Carbon::instance($plantAt)->toDateString();
    return 'email_day_count_' . $date;

}


/**
 * 邮件上次发送时间
 * @return \Illuminate\Support\Carbon
 */
function emailLastPlantAt()
{
    $startDate = \Illuminate\Support\Carbon::now();
    $find = true;
    while ($find) {
        $key = emailLimitCacheKey($startDate);
        // 如果没有存在 key 则跳出
        // 如果存在,且没超过300 则跳出
        if (cache()->has($key) && cache($key) >= 299) {
            $startDate = $startDate->addDay();
        } else {
            $find = false;
        }
    }

    $lastEmail = \App\Models\SendingEmail::orderBy('plant_at', 'desc')->first();
    $last = $lastEmail['plant_at'];
    $last = \Illuminate\Support\Carbon::instance($last);
    if ($last->gt($startDate)) {
        return $last->addSeconds(37);
    }

    return $startDate;
}

/**
 * email 部分格式不发送邮件
 * @param $email
 * @return bool
 */
function skipSendEmail($email)
{
    $skipStr = [
        '@hidden',
        '@hotmail',
        '@outlook',
        '@msn.com'
    ];
    return \Illuminate\Support\Str::contains($email, $skipStr);
}