<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@welcome');
/*Route::get('/tail', function(){
	return view('tail');
});*/

//部署
Route::post('/deploy', 'DeployController@index')->name('deploy');
Auth::routes();

// logs
Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index')->middleware('auth');

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('users', 'UsersController');

Route::resource('members', 'MembersController', ['only' => ['index', 'show', 'create', 'store', 'update', 'edit', 'destroy']]);
Route::resource('email_templates', 'EmailTemplatesController', ['only' => ['index', 'show', 'create', 'store', 'update', 'edit', 'destroy']]);
Route::resource('sending_emails', 'SendingEmailsController', ['only' => ['index', 'show', 'create', 'store', 'update', 'edit', 'destroy']]);
Route::resource('email_types', 'EmailTypesController', ['only' => ['index', 'show', 'create', 'store', 'update', 'edit', 'destroy']]);
Route::resource('tags', 'TagsController', ['only' => ['index', 'show', 'create', 'store', 'update', 'edit', 'destroy']]);

// 发送邮件
Route::get('/email_send', 'EmailSendController@create')->name('email_send.create');
Route::get('/email_send/mailgun_hook', 'EmailSendController@mailgunHook')->middleware('mailgun')->name('email_send.mailgun_hook');
Route::get('/email_send/search_member', 'EmailSendController@searchMember')->name('email_send.search_member');
Route::get('/email_send/search_product', 'EmailSendController@searchProduct')->name('email_send.search_product');
Route::any('/email_send/check_style', 'EmailSendController@checkStyle')->name('email_send.check_style');
Route::get('/email_send/retry', 'EmailSendController@retry')->name('email_send.retry');
Route::get('/email_send/retry_all', 'EmailSendController@retryAll')->name('email_send.retry_all');
Route::post('/email_send', 'EmailSendController@store')->name('email_send.store');

Route::get('/failed_jobs', 'FailedJobsController@index')->name('failed_jobs.index');
Route::get('/failed_jobs/redo', 'FailedJobsController@redo')->name('failed_jobs.redo');
Route::get('/failed_jobs/flush', 'FailedJobsController@flush')->name('failed_jobs.flush');
Route::get('/failed_jobs/{job}', 'FailedJobsController@show')->name('failed_jobs.show');
Route::delete('/failed_jobs/{job}', 'FailedJobsController@destroy')->name('failed_jobs.destroy');

Route::resource('products', 'ProductsController', ['only' => ['index', 'show', 'create', 'store', 'update', 'edit', 'destroy']]);

Route::resource('spiders', 'SpidersController', ['only' => ['index', 'show', 'create', 'store', 'update', 'edit', 'destroy']]);
Route::get('/spider_cookies', 'SpidersController@cookies')->name('spider_cookies');
Route::post('/spider_cookies', 'SpidersController@storeCookies')->name('spider_cookies.store');
Route::get('/spider_log', 'SpidersController@spider_log')->name('spider_log');
Route::resource('crawl_user_emails', 'CrawlUserEmailsController', ['only' => ['index', 'show', 'create', 'store', 'update', 'edit', 'destroy']]);

// 用于爬虫执行的登录账号信息
Route::resource('spider_accounts', 'SpiderAccountsController', ['only' => ['index', 'show', 'create', 'store', 'update', 'edit', 'destroy']]);